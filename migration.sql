CREATE TABLE declaracoes (
  id int primary key auto_increment,
  hash text not null,
  texto text not null,
  data varchar(255) not null,
  projetos text not null
);

alter table edital add column data_de_termino date null;
alter table edital add column data_de_inicio date null;
alter table edital add column localizacao_fisica varchar(255) null;
alter table edital add column valor_bolsa_discente int not null;
alter table edital add column total_bolsa_discente int not null default 0;
alter table registros add column localizacao_fisica varchar(255) null;
alter table registros add column consideracoes varchar(255) null;