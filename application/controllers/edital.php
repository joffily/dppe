<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Edital extends CI_Controller {

	/**
	* Layout default utilizado pelo controlador.
	*/
	public $layout = 'principal';

	/**
	* Titulo default.
	*/
	public $title = 'DPPE Online - Edital';

	/**
	* Definindo os css default.
	*/
	public $css = array('bootstrap-theme.min', 'bootstrap.min');

	/**
	* Carregando os js default.
	*/
	public $js = array('jquery.min', 'bootstrap.min');



	public function __construct(){
		parent::__construct();
		if(!$this->session->userdata('session_id') || !$this->session->userdata('logado')){
			redirect(site_url('login'));
		}

		$this->load->library('grocery_CRUD');
	}

	public function _dppe_output($output = null){
		$this->load->view('exibir', $output);
	}


	public function index(){
		$this->edital_gerenciamento();
		// $this->_dppe_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
		// $this->load->view('exibir.php', $output);
	}



	public function edital_gerenciamento(){
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('edital');
			$crud->set_subject('Edital');
			// Define os campos obrigatórios
			$crud->required_fields(
				'numero',
				'ano',
				'dt_publicacao',
				'objetivos_gerais',
				'total_bolsa',
				'valor_bolsa',
				'id_tipo_edital',
				'total_bolsa_discente',
				'valor_bolsa_discente',
				'data_de_inicio',
				'data_de_termino'
			);

			// Listagem dos objetos na página principal
			$crud->columns('numero','ano','dt_publicacao','objetivos_gerais');

			// Nomeia os campos
			$crud->display_as('numero','Número')
				 ->display_as('dt_publicacao','Data Publicação')
				 ->display_as('id_programa_financiador','Programa Financiador')
				 ->display_as('id_financiadora_recurso','Financiadora Recurso')
				 ->display_as('id_tipo_edital','Tipo de Edital')
				 ->display_as('total_bolsa','Total de Bolsas para Servidor')
				 ->display_as('valor_bolsa','Valor de Bolsa para servidor')
				 ->display_as('taxa_bancada','Valor de Taxa de Bancada')
				 ->display_as('objetivos_gerais','Descrição')
				 ->display_as('edital_url','Arquivo Digital')
				 ->display_as('total_bolsa_discente', 'Total de Bolsas para Discente')
				 ->display_as('valor_bolsa_discente', 'Valor de bolsa pra Discente')
				 ->display_as('localizacao_fisica', 'Localização no Arquivo')
				 ->display_as('data_de_inicio', 'Data de Início')
				 ->display_as('data_de_termino', 'Data de Término');

			// Cria um relacionamento entre os campos
			$crud->set_relation('id_programa_financiador','programa_financiador','descricao');
			$crud->set_relation('id_tipo_edital','tipo_edital','descricao');
			$crud->set_relation('id_financiadora_recurso','financiadora_recurso','descricao');
			$crud->set_relation('ano','ano_edital','ano', null, 'ano DESC');

			// Adiciona os campos a tela na ordem sugerida
			$crud->add_fields(
				'numero',
				'id_tipo_edital',
				'ano',
				'dt_publicacao',
				'data_de_inicio',
				'data_de_termino',
				'id_programa_financiador',
				'id_financiadora_recurso',
				'total_bolsa',
				'valor_bolsa',
				'total_bolsa_discente',
				'valor_bolsa_discente',
				'taxa_bancada',
				'edital_url',
				'localizacao_fisica',
				'objetivos_gerais'
			);

			// Adiciona os campos para edição
			$crud->edit_fields(
				'numero',
				'id_tipo_edital',
				'ano',
				'dt_publicacao',
				'data_de_inicio',
				'data_de_termino',
				'id_programa_financiador',
				'id_financiadora_recurso',
				'total_bolsa',
				'valor_bolsa',
				'total_bolsa_discente',
				'valor_bolsa_discente',
				'taxa_bancada',
				'edital_url',
				'localizacao_fisica',
				'objetivos_gerais'
			);

			$crud->set_field_upload('edital_url','assets/uploads/files');

			$crud->add_action('Visualizar', '', 'edital/visualizaredital','ui-icon-document');

			$crud->unset_read();
			$crud->unset_export();
			$crud->unset_print();
			$crud->unset_delete();

			$output = $crud->render();

			$this->_dppe_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}



	public function visualizaredital($numero = null){

		if($numero != null){
			//participantes
			$this->load->model("edital_model");
			$edital = $this->edital_model->pesquisarEditalId($numero);

			$edano = $this->edital_model->pesquisarEditalAno($edital[0]['ano']);
			$edtipo = $this->edital_model->pesquisarEditalTipo($edital[0]['id_tipo_edital']);
			$edprograma = $this->edital_model->pesquisarEditalPrograma($edital[0]['id_programa_financiador']);
			$edfrecurso = $this->edital_model->pesquisarEditalFRecurso($edital[0]['id_financiadora_recurso']);

			$val = array();

			$trat = array(
				"ano" =>  $edano[0]['ano'],
				"tipo" => $edtipo[0]['descricao'],
				"programa" => $edprograma[0]['descricao'],
				"frecurso" => $edfrecurso[0]['descricao']
			);
			array_push($val, $trat);

			//registro
			$this->load->model("registro_model");
			$registros = $this->registro_model->pesquisarRegistroEdital($numero);

			$valores = array();

			foreach($registros as $registo){
				$datafim = $registo->dt_fim;
				if($datafim == null){
					$datafim = "--";
				}

				$tratado = array(
					"id" =>  $registo->id,
					"titulo" =>  $registo->titulo,
					"dt_inicio" => $registo->dt_inicio,
					"dt_fim" => $datafim
				);
				array_push($valores, $tratado);

			}

			if($edital != false){
				$dado = array(
					"registro" => $valores,
					"edital" => $edital,
					"dadosextras" => $val
				);

				$this->load->view("edital/edital", $dado);

		}else{
			$this->load->view("principal");
		}

		}

	}

}
