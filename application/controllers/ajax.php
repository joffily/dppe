<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

	/**
	* Layout default utilizado pelo controlador.
	*/
	public $layout = 'limpo';

	public function __construct(){
		parent::__construct();
		// if(!$this->session->userdata('session_id') || !$this->session->userdata('logado')){
		// 	redirect(site_url('login'));
		// }

	}

	public function getAlunos(){		
		$this->load->model("ajax_model");
		$dados = $this->input->post('keyword');
		//var_dump($dados);
		//if(is_numeric($dados)){
		if($this->tipoDigitado($dados)){
			$resultado = $this->ajax_model->getAlunosMatricula($dados);	
		}else{
			$resultado = $this->ajax_model->getAlunosNome($dados);	
		}
		foreach ($resultado as $value) {
			echo '<li style=list-style:none;" onclick="set_item_participante(\''.str_replace("'", "\'", $value->nome)." - ".$value->matricula.'\')">'.$value->matricula. " - ".$value->nome.'</li>';
		}
	}
	
	public function getEdital(){		
		$this->load->model("ajax_model");
		$dados = $this->input->post('keyword');
	
		if($this->tipoDigitado($dados)){
			$resultado = $this->ajax_model->getEditalNumero($dados);	
		}else{
			$resultado = $this->ajax_model->getEditalObjetivo($dados);	
		}
		foreach ($resultado as $value) {
			//echo $value->nome ."-". $value->matricula."<br>" ;
			
			echo '<li style=list-style:none;" onclick="set_item_edital(\''.$value->id."-".$value->numero."/".$value->ano.'\')">'.$value->numero."/".$value->ano." - ".substr($value->objetivos_gerais, 0, 150).'<p></li>';
		}
	}


	public function getProfessor(){		
		$this->load->model("ajax_model");
		$dados = $this->input->post('keyword');
	
		if($this->tipoDigitado($dados)){
			$resultado = $this->ajax_model->getProfessorMatricula($dados);	
		}else{
			$resultado = $this->ajax_model->getProfessorNome($dados);	
		}
		foreach ($resultado as $value) {
			//echo $value->nome ."-". $value->matricula."<br>" ;
			
			echo '<li style=list-style:none;" onclick="set_item_professor(\''.str_replace("'", "\'", $value->nome)." - ".$value->siape.'\')">'.$value->siape. " - ".$value->nome.'</li>';
		}
	}

	private function tipoDigitado($text){
	    return preg_match('/^-?[0-9]+$/', (string)$text) ? true : false;
	}

	public function getProfessorJson($matricula = null){
		$this->load->model("ajax_model");
		
		if($matricula == null){
			return;
		}

		$resultado = $this->ajax_model->getUmProfessor($matricula);

		
		if($resultado == null){
			return;
		}

		echo json_encode($resultado);
		
	}

	public function setEditalSessao(){

		//pegar via post
		$dadoEdital = $this->input->post("dadoEdital");
		// var_dump($dadoEdital);
				
		//pega o id do edital
		$dadoEdital = explode("-", $dadoEdital);
		$id_edital = $dadoEdital[0];
		$this->load->model("edital_model");
		$edt = $this->edital_model->pesquisarEditalNumero($id_edital);

				
		//Esta linha serve para permitir que produtos com acentuação no nome sejam aceitos.
		$this->cart->product_name_rules = "'\d\D'";

		//echo "adicionado";
		$data = array(
		 	"id" => $edt[0]['id'],
		 	"qty" => $edt[0]['ano'],
		 	"price" => 1.0,
		 	"name" => "edital",
		 	"options" => array("objetivos_gerais" => substr($edt[0]['objetivos_gerais'],0, 80))
		);			

		$this->cart->insert($data);

	}

	public function exibe(){
		print_r($this->input->post());
	}

	public function removeEdital($id){		

		if(isset($id)){
			$data = array(
			'rowid'   => $id,
			'qty'     => 0
			);

			$this->cart->update($data); 			
		}
		redirect(site_url()."/registro/cadastro");


	}


	public function setProfessorSessao(){

		$this->load->model("ajax_model");

		//pegar via post
		$dadosProfessor = $this->input->post("dadosProfessor");
		//var_dump($dadosProfessor);
		
		//pega o dados do professor
		$dados = explode("-", $dadosProfessor['nome']);
		$siape = trim($dados[1]);
		$nome = trim($dados[0]);
		
		//pegar papel
		$papel = trim($dadosProfessor['papel']);
		$papel = $this->ajax_model->getPapel($papel);
		//var_dump($papel[0]->descricao);
		$data = trim($dadosProfessor['data']);
		
		//professor		
		$professor = $this->ajax_model->getUmProfessor($siape);		
						
		//Esta linha serve para permitir que produtos com acentuação no nome sejam aceitos.
		$this->cart->product_name_rules = "'\d\D'";

		//echo "adicionado";
		$data = array(
		 	"id" => $siape,
		 	"qty" => 1,
		 	"price" => 1.0,
		 	"name" => "professor",
		 	"options" => array("data" => $data, "papel" => $papel[0]->descricao, "nome" => $professor[0]->nome)
		);			

		$this->cart->insert($data);

	}


	public function removeProfessor($id){		

		if(isset($id)){
			$data = array(
			'rowid'   => $id,
			'qty'     => 0
			);

			$this->cart->update($data); 
		}

		redirect(site_url()."/registro/cadastro");
	}	

	public function removeParticipante($id){		

		if(isset($id)){
			$data = array(
			'rowid'   => $id,
			'qty'     => 0
			);

			$this->cart->update($data); 
		}
		redirect(site_url()."/registro/cadastro");
	}	

	public function setParticipanteSessao(){

		$this->load->model("ajax_model");

		//pegar via post
		$dadosParticipante = $this->input->post("dadosParticipante");
		// var_dump($dadosParticipante);;		
		
		//pega o dados do participante
		$dados = explode("-", $dadosParticipante['nome']);
		$matricula = trim($dados[1]);
		$nome = trim($dados[0]);
		
		//pegar papel
		$papel = trim($dadosParticipante['papel']);
		$papel = $this->ajax_model->getPapel($papel);
		//var_dump($papel[0]->descricao);
		$data = trim($dadosParticipante['data']);
		
		//participante
		$participante = $this->ajax_model->getUmAluno($matricula);
						
		//Esta linha serve para permitir que produtos com acentuação no nome sejam aceitos.
		$this->cart->product_name_rules = "'\d\D'";

		//echo "adicionado";
		$data = array(
		 	"id" => $matricula,
		 	"qty" => 1,
		 	"price" => 1.0,
		 	"name" => "participante",
		 	"options" => array("data" => $data, "papel" => $papel[0]->descricao, "nome" => $participante[0]->nome)
		);			
		
		$this->cart->insert($data);

	}

	public function getSessao(){
		echo "<pre>";
		print_r($this->cart->contents());
		echo "</pre>";
	}

	public function limparSessao(){
			
		$this->cart->destroy();
	}

	public function teste(){
		$this->load->model("registro_model");
		$this->load->view("teste");
	}
}
