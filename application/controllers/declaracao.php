<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Declaracao extends CI_Controller {
  // public $layout = 'principal';
  public $layout = 'publico';
  public $title = 'DPPE Online - Declaração Online';
  public $css = array('bootstrap-theme.min', 'bootstrap.min', 'styletablehand', 'jquery-ui', 'jquery-ui.min');


  public function __construct() {
    parent::__construct();
    $this->load->model('dppe_model');
    $this->load->model("participante_model");
    $this->load->model("declaracao_model");
    $this->hash = substr(hash('sha256', date('y-m-d h-m-s'), false), 0, 6);


    setlocale(LC_ALL, "pt_BR", "pt_BR.iso-8859-1", "pt_BR.utf-8", "portuguese");
    date_default_timezone_set('America/Sao_Paulo');
  }

  public function index() {
    $this->load->view("declaracao/index");
  }

  public function validar() {
    $dppe = $this->dppe_model->getDppe();

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $hash = $_POST['hash'];
      //$captcha_data = $_POST['g-recaptcha-response'];

      if (isset($hash) && strlen($hash) == 6) {
        // $resposta = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcpIyITAAAAABwEOOT_dgT3MYFPLTb9_UH-LYyC&response=".$captcha_data."&remoteip=".$_SERVER['REMOTE_ADDR']));
        $resposta = TRUE;

        if ($resposta) {
          $declaracao = $this->declaracao_model->get_by_hash($hash)[0];

          $dados = array(
            "hash" => $declaracao['hash'],
            "projetos" => unserialize($declaracao['projetos']),
            "texto" => $declaracao['texto'],
            "data" => $declaracao['data'],
            "dppe" => $dppe
          );

          $this->load->view("declaracao/declaracao_publica", $dados);
        } else {
          $this->session->set_flashdata("erro", "Por favor informe o Captcha");
          redirect('/declaracao/validar', 'refresh');
        }
      } else {
        $this->session->set_flashdata("erro", "Código verificador inválido");
        redirect('/declaracao/validar', 'refresh');
      }
    } else {
      $this->load->view("declaracao/validar");
    }
  }

  public function emitir() {
    $dppe = $this->dppe_model->getDppe();

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $matricula = $_POST['matricula'];
      //$captcha_data = $_POST['g-recaptcha-response'];

      if (isset($matricula) && strlen($matricula) > 5 && intval($matricula)) {
        // $resposta = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcpIyITAAAAABwEOOT_dgT3MYFPLTb9_UH-LYyC&response=".$captcha_data."&remoteip=".$_SERVER['REMOTE_ADDR']));
	$resposta = TRUE;

        if ($resposta) {
          $participantes = $this->participante_model->pesquisarParticipanteMatricula($matricula);

          //registro
          $this->load->model("registro_model");
          $registros = $this->registro_model->buscaRegistro($participantes[0]['id']);

            if (count($registros) > 0) {
              $valores = array();
              foreach($registros as $registo){
                $dados = $this->registro_model->pesquisarRegistroNumero($registo->id_registro);
                $datafim = $registo->dt_fim;
                if($datafim == null){
                  $datafim = "--";
                }

                $this->load->model("edital_model");
                $regedital = $this->edital_model->pesquisarEditalId($dados[0]['id_edital']);
                $edano = $this->edital_model->pesquisarEditalAno($regedital[0]['ano']);

                $tratado = array(
                  "papel" => $registo->id_papel,
                  "titulo" => $dados[0]['titulo'],
                  "dt_inicio" => $registo->dt_inicio,
                  "dt_fim" => $datafim,
                  "numeroedital" => $regedital[0]['numero'],
                  "anoedital" => $edano[0]['ano']
                );
                array_push($valores, $tratado);

              }

              // Hash
              $hash = $this->hash;
              // Montando o texto
              $texto = "Eu, ".$dppe[1]->dados.", coordenador do ".$dppe[2]->dados." do ".$dppe[5]->dados.", declaro
              que <b> ".$participantes[0]['nome']."</b>, matricula: ".$participantes[0]['matricula'].".
              <p>Possuí o(s) seguinte(s) registro(s) nesse Departamento:</p></br>";
              //Montando a Data
              $data = date('y-m-d');
              $data =  "João Pessoa".utf8_encode(strftime(", %d de %B de %Y", strtotime($data)));

              //Projetos serializados
              $projetos = serialize($valores);

              $declaracao = array(
                'hash' => $hash,
                'texto' => $texto,
                'data' => $data,
                'projetos' => $projetos
              );

              $dado = array(
                "hash" => $hash,
                "projetos" => $valores,
                "texto" => $texto,
                "data" => $data,
                "dppe" => $dppe,
                "error" => ""
              );

              // Cria declaração no banco
              $this->declaracao_model->create($declaracao);

              $this->load->view("declaracao/declaracao_publica", $dado);
            } else {
              //Não foram encontrados projetos para essa matrícula
              $this->session->set_flashdata("erro", "Não foram encontrados projetos para esta matrícula");
              redirect('/declaracao/emitir', 'refresh');
            }
          } else {
            $this->session->set_flashdata("erro", "Por favor informe o Captcha");
            redirect('/declaracao/emitir', 'refresh');
          }
        } else {
          $this->session->set_flashdata("erro", "Mátricula não localizada");
          redirect('/declaracao/emitir', 'refresh');
        }
      } else {
      $this->load->view("declaracao/emitir");
    } // Validação POST
  }
}
