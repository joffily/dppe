<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	* Layout default utilizado pelo controlador.
	*/
	public $layout = 'principal';

	/**
	* Titulo default.
	*/
	public $title = 'DPPE Online - Login';

	/**
	* Definindo os css default.
	*/
	public $css = array('bootstrap.min', 'form_login');

	/**
	* Carregando os js default.
	*/
	public $js = array('jquery.min');

	public function __construct(){
		parent::__construct();
	}

	public function index(){
		if(!$this->session->userdata('session_id') || !$this->session->userdata('logado')){
			$this->load->view('login');
		}else{
			redirect("principal/index");
		}

	}

	public function logar(){

		$this->load->library("form_validation");

		$this->form_validation->set_rules('login', 'Login', 'required');
		$this->form_validation->set_rules('senha', 'Senha', 'required');

		if($this->form_validation->run() == false){
			$this->session->set_flashdata("erro", "Erro no preenchimento do usuário ou senha");
			redirect("login/index");
		}else{

			$log = array(
				"login" => $this->input->post("login"),
				"senha" => md5($this->input->post("senha"))
			);

			//carregar o banco de dados- esta automatico
			$this->load->model("login_model");
			$resultado = $this->login_model->verifica($log);
			if($resultado){
				//pega os dados do usuario
				$usuario = $this->login_model->getUsuario($this->input->post("login"));

				//criar a sessão aqui
				$data = array(
					'usuario' => $usuario->login,
					'nome' => $usuario->nome,
					'logado' => TRUE,
					'administrador' => $usuario->administrador
				);

				$this->session->set_userdata($data);
				//$this->load->view('principal');
				redirect("principal/index");

			}else{
				$this->session->set_flashdata("erro", "Login ou Senha inválidos");
				redirect("login/index");
			}

		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(site_url("login"));
	}


}
