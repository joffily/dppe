<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pesquisa extends CI_Controller{
	/**
	* Layout default utilizado pelo controlador.
	*/
	public $layout = 'principal';
	 
	/**
	* Titulo default.
	*/
	public $title = 'DPPE Online - Consulta';
	 
	/**
	* Definindo os css default.
	*/

	public $css = array('bootstrap-theme.min', 'bootstrap.min', 'styletablehand', 'jquery-ui', 'jquery-ui.min');
	 
	/**
	* Carregando os js default.
	*/
	public $js = array('jquery-1.11.1.min', 'bootstrap.min', 'jquery.dataTables.min','dataTables.bootstrap','jquery-ui-1.10.3.min', 'jquery-ui', 'jquery-ui.min');

	public function __construct(){
		parent::__construct();
		if(!$this->session->userdata('session_id') || !$this->session->userdata('logado')){
			redirect(site_url('login'));
		}
		$this->load->model( 'pesquisa_model' );
	}
	
	public function index(){
		$this->load->view("principal");	
	}


	public function professor(){
		
		$this->load->helper("form");

		//preencher select - titularidade
		$titularidade = $this->pesquisa_model->titularidade();
		$titularidadeTratado = array("0" => "Titularidade");
		foreach($titularidade as $key => $linha) {		
			$titularidadeTratado[$linha['titularidade']] =  $linha['titularidade'];
		}

		//preencher select - situacao
		$situacao = $this->pesquisa_model->situacaoProfessor();
		$situacaoTratado = array("0" => "Situação");
		foreach($situacao as $key => $linha) {		
			$situacaoTratado[$linha['situacao']] =  $linha['situacao'];
		}

		//preencher select - instituicao
		$instituicao = $this->pesquisa_model->instituicaoProfessor();
		$instituicaoTratado = array("0" => "Instituição");
		foreach($instituicao as $key => $linha) {		
			$instituicaoTratado[$linha['instituicao']] =  $linha['instituicao'];
		}

		$dados = array(
			"titularidade" => $titularidadeTratado,
			"situacao" => $situacaoTratado,
			"instituicao" => $instituicaoTratado				

		);
		
		$this->load->view("pesquisa/professor", $dados);

	}


	public function aluno(){
		
		$this->load->helper("form");

		//preencher select - curso
		//preencher os selects			
		$cursos = $this->pesquisa_model->curso();
		$cursoTratado = array('0' => 'Curso');
		foreach($cursos as $key => $linha) {		
			$cursoTratado[$linha['curso']] =  $linha['curso'];
		}
		
		
		//preencher select - situacao matricula
		$situacao_matricula = $this->pesquisa_model->matricula();
		$situacao = array('0' => 'Situacao da Matrícula');
		foreach($situacao_matricula as $key => $linha) {		
			$situacao[$linha['situacao_matricula']] =  $linha['situacao_matricula'];
		}

		

		$dados = array(
			"curso" => $cursoTratado,
			"situacao" => $situacao

		);

		$this->load->view("pesquisa/aluno", $dados);

	}


	public function pesquisaAluno(){
		$this->load->helper("form");

		//preencher select - curso
		//preencher os selects			
		$cursos = $this->pesquisa_model->curso();
		$cursoTratado = array('0' => 'Curso');
		foreach($cursos as $key => $linha) {		
			$cursoTratado[$linha['curso']] =  $linha['curso'];
		}
		
		
		//preencher select - situacao matricula
		$situacao_matricula = $this->pesquisa_model->matricula();
		$situacao = array('0' => 'Situacao da Matrícula');
		foreach($situacao_matricula as $key => $linha) {		
			$situacao[$linha['situacao_matricula']] =  $linha['situacao_matricula'];
		}

		//capturar dados do Post
		$nome = $this->input->post('nome');
		$matricula = $this->input->post('matricula');
		$situacao_matricula = $this->input->post('situacao_matricula');
		$curso = $this->input->post('curso');

		if(!empty($matricula)){
			//pesquisa por matricula
			$alunos = $this->pesquisa_model->pesquisaMatriculaAluno($matricula);
		}else {
			//pesquisa por nome
			$alunos = $this->pesquisa_model->pesquisaNomeAluno($nome, $situacao_matricula, $curso);
		}

		
		$dados = array(
			'curso' => $cursoTratado,
			'situacao' => $situacao,
			'alunos' => $alunos
		);

		$this->load->view("pesquisa/aluno", $dados);
		

	}

	public function pesquisaProfessor(){
		$this->load->helper("form");

		//preencher select - titularidade
		$titularidade = $this->pesquisa_model->titularidade();
		$titularidadeTratado = array("0" => "Titularidade");
		foreach($titularidade as $key => $linha) {		
			$titularidadeTratado[$linha['titularidade']] =  $linha['titularidade'];
		}

		//preencher select - situacao
		$situacao = $this->pesquisa_model->situacaoProfessor();
		$situacaoTratado = array("0" => "Situação");
		foreach($situacao as $key => $linha) {		
			$situacaoTratado[$linha['situacao']] =  $linha['situacao'];
		}

		//preencher select - instituicao
		$instituicao = $this->pesquisa_model->instituicaoProfessor();
		$instituicaoTratado = array("0" => "Instituição");
		foreach($instituicao as $key => $linha) {		
			$instituicaoTratado[$linha['instituicao']] =  $linha['instituicao'];
		}

		//capturar dados do Post
		$nome = $this->input->post('nome');
		$matricula = $this->input->post('matricula');
		$titularidade = $this->input->post('titularidade');
		$situacao = $this->input->post('situacao');
		$instituicao = $this->input->post('instituicao');

		if(!empty($matricula)){
			//pesquisa por matricula
			$professores = $this->pesquisa_model->pesquisaMatriculaProfessor($matricula);
		}else{
			//pesquisa por nome
			$professores = $this->pesquisa_model->pesquisaNomeProfessor($nome, $titularidade, $situacao, $instituicao);
		}


		$dados = array(
			"titularidade" => $titularidadeTratado,
			"situacao" => $situacaoTratado,
			"instituicao" => $instituicaoTratado,
			"professores" => $professores	 			

		);

		$this->load->view("pesquisa/professor", $dados);
	
	}

	
	public function exibirAluno($id){
		$this->load->helper("form");

		//pesquisa id do aluno
		$aluno = $this->pesquisa_model->pesquisaIdAluno($id);
		
		$dados = array(
			'aluno' => $aluno
		);

		$this->load->view("pesquisa/exibirAluno", $dados);


	}

	public function exibirProfessor($id){
		$this->load->helper("form");

		//pesquisa id do aluno
		$professor = $this->pesquisa_model->pesquisaIdProfessor($id);
		
		$dados = array(
			'professor' => $professor
		);

		$this->load->view("pesquisa/exibirProfessor", $dados);


	}


}
	