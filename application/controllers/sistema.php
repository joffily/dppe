<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sistema extends CI_Controller {

	/**
	* Layout default utilizado pelo controlador.
	*/
	public $layout = 'principal';
	 
	/**
	* Titulo default.
	*/
	public $title = 'DPPE Online - Usuário';
	 
	/**
	* Definindo os css default.
	*/
	public $css = array('bootstrap-theme.min', 'bootstrap.min');
	 
	/**
	* Carregando os js default.
	*/
	public $js = array('jquery.min', 'bootstrap.min');


	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('session_id') || !$this->session->userdata('logado') || $this->session->userdata('administrador') == 'N'){
			redirect(site_url('login'));
		}
		$this->load->helper('url');
		$this->load->library('grocery_CRUD');
	}

	public function _dppe_output($output = null){
		$this->load->view('exibir',$output);
	}


	public function index(){
		$this->usuario_gerenciamento();
	}

	public function usuario_gerenciamento()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('usuarios');

			$crud->set_subject('Usuário');			
			$crud->required_fields('login', 'nome', 'email', "ativo", "senha", "administrador");
			$crud->columns('login', 'nome', 'email');

			$crud->field_type('senha','password');
			$crud->field_type('ativo', 'dropdown',array('0' => 'Não', '1' => 'Sim'));
			$crud->field_type('administrador', 'dropdown',array('N' => 'Não', 'S' => 'Sim'));
			
			$crud->callback_before_insert(array($this,'encrypt_password'));
			$crud->callback_before_update(array($this,'encrypt_password'));

			if ($crud->getState() == 'read') {
    		   	$crud->field_type('senha', 'hidden');
   			}

			$crud->unset_export();
			$crud->unset_print();

			$output = $crud->render();

			$this->_dppe_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function encrypt_password($post_array, $primary_key = null){
	  
	    $this->load->helper('security');
	    $post_array['senha'] = do_hash($post_array['senha'], 'md5');
	    return $post_array;	   
    }

	public function dados_dppe_gerenciamento(){
	
		$crud = new grocery_CRUD();

		$crud->set_theme('datatables');
		$crud->set_table('dppe');
		$crud->unset_edit_fields('atributo');

		$crud->set_subject('DPPE Dados');			
		$crud->required_fields('atributo', 'dados');
		$crud->columns('atributo', 'dados');

		$crud->unset_delete();

		$output = $crud->render();

		$this->_dppe_output($output);		
	}

	public function financiadora_recurso_gerenciamento(){
	
		$crud = new grocery_CRUD();

		$crud->set_theme('datatables');
		$crud->set_table('financiadora_recurso');
		
		$crud->set_subject('Financiadora de Recurso');			
		$crud->required_fields('descricao');
		$crud->display_as('descricao','Descrição');

		$crud->unset_delete();

		$output = $crud->render();

		$this->_dppe_output($output);		
	}

	public function programa_financiador_gerenciamento(){
	
		$crud = new grocery_CRUD();

		$crud->set_theme('datatables');
		$crud->set_table('programa_financiador');
		
		$crud->set_subject('Programa Financiador');			
		$crud->required_fields('descricao');
		$crud->display_as('descricao','Descrição');

		$crud->unset_delete();

		$output = $crud->render();

		$this->_dppe_output($output);		
	}

	public function papel_gerenciamento(){
	
		$crud = new grocery_CRUD();

		$crud->set_theme('datatables');
		$crud->set_table('papeis');
		
		$crud->set_subject('Papéis');			
		$crud->required_fields('descricao');
		$crud->display_as('descricao','Descrição');

		$crud->unset_delete();

		$output = $crud->render();

		$this->_dppe_output($output);		
	}
	
	public function tipo_edital_gerenciamento(){
	
		$crud = new grocery_CRUD();

		$crud->set_theme('datatables');
		$crud->set_table('tipo_edital');
		
		$crud->set_subject('Tipo do Edital');			
		$crud->required_fields('descricao');
		$crud->display_as('descricao','Descrição');

		$crud->unset_delete();

		$output = $crud->render();

		$this->_dppe_output($output);		
	}

	public function ano_edital_gerenciamento(){
	
		$crud = new grocery_CRUD();

		$crud->set_theme('datatables');
		$crud->set_table('ano_edital');
		
		$crud->set_subject('Ano do Edital');			
		$crud->required_fields('descricao');
		$crud->display_as('descricao','Descrição');

		$crud->unset_delete();

		$output = $crud->render();

		$this->_dppe_output($output);		
	}
}