<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registro extends CI_Controller {

	/**
	* Layout default utilizado pelo controlador.
	*/
	public $layout = 'principal';

	/**
	* Titulo default.
	*/
	public $title = 'DPPE Online - Registro';

	/**
	* Definindo os css default.
	*/
	public $css = array('bootstrap-theme.min', 'bootstrap.min', 'styles-modal','datepicker', 'styletablehand', 'jquery-ui', 'jquery-ui.min', "app");

	/**
	* Carregando os js default.
	*/
	public $js = array('jquery-1.11.1.min', 'bootstrap.min', 'jquery.dataTables.min','dataTables.bootstrap','jquery-ui-1.10.3.min',
						'ckeditor/ckeditor','bootstrap-datepicker', 'jquery-ui', 'jquery-ui.min', 'jquery.confirm.min', 'jquery.Storage', 'registro');

	public function __construct(){
		parent::__construct();
		if(!$this->session->userdata('session_id') || !$this->session->userdata('logado')){
			redirect(site_url('login'));
			$this->cart->destroy();
		}
		$this->load->model('dppe_model');
	}


	public function index(){
		$this->load->view("principal");

	}

	public function mostra($id){

		// $this->$title = 'DPPE Online - Registro > Lista os Registros';

		$this->load->model("registro_model");
		// $registro = $this->readfile(filename);

		$registro = $this->registro_model->pesquisarRegistroNumero($id);

		$edital = array();
		if($registro){

			$this->load->model("edital_model");
			$regedital = $this->edital_model->pesquisarEditalId($registro[0]['id_edital']);
			$edano = $this->edital_model->pesquisarEditalAno($regedital[0]['ano']);

			$tratado = array(
				"id" => $regedital[0]['id'],
				"numero" => $regedital[0]['numero'],
				"ano" => $edano[0]['ano']
			);
			array_push($edital, $tratado);

			//pesquisar participantes do registro
			$participante =  $this->registro_model->pegarPart($id);

			$dados = array(
				"registro" => $registro,
				"participantes" => $participante,
				"edital" => $edital
			);

			$this->load->view("registro/registro", $dados);

		}else{
			$this->listarregistro();
		}

	}

	public function listarregistro(){

		$this->load->model("registro_model");
		$registro = $this->registro_model->buscarTodosRegistros();

		$dados = array("registros" => $registro);

		$this->load->view("registro/registros", $dados);


	}

	public function cadastro(){

		$this->load->helper("form", 'ckeditor');

		//preencher select
		$this->load->model("registro_model");
		$papeis = $this->registro_model->papeisProjeto();

		$option = "\<select\ name\=papel[] id=papel class='form-control'>\<option value='erro' class='form-control'>Definir o Papel\<\/option\>";
		foreach($papeis as $linha) {
			$option .= "\<option value='$linha->id'\>$linha->descricao\<\/option\>";
		}
		$option .="\<\/select\>";



        // Array com as configurações pra essa instância do CKEditor ( você pode ter mais de uma )
        // $data['ckeditor_texto1'] = array
        // (
        //     //id da textarea a ser substituída pelo CKEditor
        //     'id'   => 'texto1',

        //     // caminho da pasta do CKEditor relativo a pasta raiz do CodeIgniter
        //     'path' => 'assets/js/ckeditor',

        //     // configurações opcionais
        //     'config' => array
        //     (
        //         'toolbar' => "Full",
        //         'width'   => "400px",
        //         'height'  => "100px",
        //     )
        // );


		// $dados = array(
		// 	"opcoes" => $option,
		// 	"data" => $data
		// );
		// echo "<pre>";
		// var_dump($dados);
		// echo "</pre>";

		$dados = array(
			"opcoes" => $option
		);
		$this->load->view('registro/cadastro', $dados);

	}


	public function novo(){

		$this->load->helper("form");

		$this->load->library("form_validation");

		$this->form_validation->set_rules('titulo', 'Título', 'required');
		$this->form_validation->set_rules('dt_inicio', 'Data Início', 'required|valid_date');
		$this->form_validation->set_rules('dt_fim', 'Data Final', 'required|valid_date');
		$this->form_validation->set_rules('consideracoes', 'Considerações', 'required|valid_date');
        $config['upload_path'] = './assets/uploads/';
        $config['upload_path_raw'] = 'assets/uploads/';
        $config['allowed_types'] = 'pdf|doc|docx';
        $config['max_size'] = '20000';
        $this->load->library('upload', $config);


		$this->load->model("registro_model");

		if($this->form_validation->run() == false) {
			$this->load->view("registro/cadastro");
			$this->session->set_flashdata("erro", "Erro no preenchimento do Registro");
		} else {
			$this->load->model("participante_model");

			//tratar o campo data
			$dt_inicio = $this->input->post("dt_inicio");
			$dt_fim = $this->input->post("dt_fim");

			$dt_inicio = explode("/",$dt_inicio);
			$dt_inicio = $dt_inicio[2].'-'.$dt_inicio[0].'-'.$dt_inicio[1];

			$dt_fim = explode("/",$dt_fim);
			$dt_fim = $dt_fim[2].'-'.$dt_fim[0].'-'.$dt_fim[1];

			//pegar dados da sessão - Edital
			$id_edital = "";
			foreach ($this->cart->contents() as $items){
				if($items['name'] == "edital"){
					$id_edital = $items['id'];
				}
			}

			// dados do registro
			$registro = array(
				"titulo" => $this->input->post("titulo"),
				"descricao" => $this->input->post("descricao"),
				"dt_inicio" => $dt_inicio,
				"dt_fim" => $dt_fim,
				"id_edital" => $id_edital,
				"localizacao_fisica" => $this->input->post("localizacao_fisica"),
				"consideracoes" => $this->input->post("consideracoes")
			);

			// Se houver upload, salva no banco
			if (isset($data)) {
				$registro["arquivo_url"] = $config['upload_path_raw'].$data["upload_data"]["file_name"];
			}

			//salvar registro
			$id_registro = $this->registro_model->salvarRegistroReturn($registro);

			//pegar dados da sessão - Professor

			$arrayProfessor = array();
			foreach ($this->cart->contents() as $items){
				if($items['name'] == "professor"){
					$siapeProfessor = $items['id'];
					// array_push($arrayProfessor, $siapeProfessor);

					//verifica se existe um professor no cadatro de participante
					$id_professor = $this->participante_model->pegaId($siapeProfessor);
					//se não existir cadastra
					if(empty($id_professor)){
						$this->load->model("integrante_model");
						$integrante = $this->integrante_model->pesquisarIntegranteProfessorMatricula($siapeProfessor);

						//array com os dados que devem ser salvos
						$dadosProfessor = array(
							"nome" => $integrante[0]['nome'],
							"matricula" => $integrante[0]['siape'],
							"curso" => $integrante[0]['instituicao'],
							"situacao_matricula" => $integrante[0]['situacao'],
							"tipo" => "PROFESSOR"
						);
						$id_professor = $this->participante_model->salvarParticipante($dadosProfessor);
					}else{
						//pega o id do participante existente
						$id_professor = $id_professor[0]['id'];
					}

					$porfessor_dt_inicio = $items['options']['data'];
					$dt_inicio = explode("/",$porfessor_dt_inicio);
					$dt_inicio = $dt_inicio[2].'-'.$dt_inicio[0].'-'.$dt_inicio[1];


					$this->load->model("ajax_model");
					$id_papel = $this->ajax_model->getIdPapel($items['options']['papel']);

					$dados = array(
						"id_registro" => $id_registro,
						"id_participante" => $id_professor,
						"id_papel" => $id_papel[0]->id,
						"dt_inicio" => $dt_inicio,
						"ativo" => "S"
					);
					$resultadoFinal = $this->registro_model->salvarRegistroParticipante($dados);
				}
			}

			//pegar dados da sessão - Participante
			$arrayParticipante = array();
			foreach ($this->cart->contents() as $items){
				if($items['name'] == "participante"){
					$matriculaParticipante = $items['id'];

					//verifica se existe um participante no cadatro
					$id_participante = $this->participante_model->pegaId($matriculaParticipante);
					//se não existir cadastra
					if(empty($id_participante)){
						$this->load->model("integrante_model");
						$integrante = $this->integrante_model->pesquisarIntegranteAlunoMatricula($matriculaParticipante);

						//array com os dados que devem ser salvos
						$dadosParticipante = array(
							"nome" => $integrante[0]['nome'],
							"matricula" => $integrante[0]['matricula'],
							"curso" => $integrante[0]['curso'],
							"situacao_matricula" => $integrante[0]['situacao_matricula'],
							"tipo" => "ALUNO"
						);
						$id_participante = $this->participante_model->salvarParticipante($dadosParticipante);
					}else{
						//pega o id do participante existente
						$id_participante = $id_participante[0]['id'];
					}

					$participante_dt_inicio = $items['options']['data'];
					$dt_inicio = explode("/",$participante_dt_inicio);
					$dt_inicio = $dt_inicio[2].'-'.$dt_inicio[0].'-'.$dt_inicio[1];

					$this->load->model("ajax_model");
					$id_papel = $this->ajax_model->getIdPapel($items['options']['papel']);

					$dados = array(
						"id_registro" => $id_registro,
						"id_participante" => $id_participante,
						"id_papel" => $id_papel[0]->id,
						"dt_inicio" => $dt_inicio,
						"ativo" => "S"
					);
					$resultadoFinal = $this->registro_model->salvarRegistroParticipante($dados);
				}
			}

		$this->cart->destroy();
		$this->session->set_flashdata("sucesso", "Registro cadastrado com Sucesso");
		redirect("registro/listarregistro");

		}

	}


	public function cadastrarParticipante(){

			$this->load->helper("form");

			$this->load->library("form_validation");


			$this->form_validation->set_rules('matricula', 'Matrícula', 'required');
			$this->form_validation->set_rules('dt_inicio', 'Data Início', 'required|valid_date');
			$this->form_validation->set_rules('dt_fim', 'Data Final', 'valid_date');


			if($this->form_validation->run() == false){

				//helper do form
				$this->load->helper("form");

				$this->load->model("registro_model");
				$papeis = $this->registro_model->papeisProjeto();
				$dados = array(
					"opcoes" => $papeis
				);

				$this->load->view("registro/inserir_participante", $dados);


			}else{
				//tratar o campo data
				$dt_inicio = $this->input->post("dt_inicio");
				$dt_fim = $this->input->post("dt_fim");

				if(!empty($dt_inicio)){
					$dt_inicio = explode("/",$dt_inicio);
					$dt_inicio = $dt_inicio[2].'-'.$dt_inicio[1].'-'.$dt_inicio[0];
				}else{
					$dt_inicio = "";
				}


				if(!empty($dt_fim)){
					$dt_fim = explode("/",$dt_fim);
					$dt_fim = $dt_fim[2].'-'.$dt_fim[1].'-'.$dt_fim[0];
				}else{
					$dt_fim = "";
				}

				//pega id do participante
				$matricula = $this->input->post("matricula");

				$this->load->model("participante_model");
				$id_participante = $this->participante_model->pegaId($matricula);

				//checkbox
				if($this->input->post("ativo") == "ativo"){
					$ativo = "S";
				}else{
					$ativo = "N";
				}

				//pega id do registro
				$registro = $this->input->post("titulo");

				$this->load->model("registro_model");
				$id_registro = $this->registro_model->pesquisarRegistroCodigo($registro);

				$dados = array(
						"id_projeto" => $id_registro[0]['codigo'],
						"id_participante" => $id_participante[0]['Id'],
						"papel" => $this->input->post("papel"),
						"dt_inicio" => $dt_inicio,
						"dt_fim" => $dt_fim,
						"ativo" => $ativo
				);

				$resultado = $this->registro_model->salvarRegistroParticipante($dados);


				if($resultado){
					$this->load->view("registro/registro_sucesso");

				}else{
					$this->load->view("registro/erro");
					$this->load->view("registro/cadastro");
				}

			}

	}
	public function inserirParticipante(){

		//codigo aqui

		//helper do form
		$this->load->helper("form");

		$this->load->model("registro_model");
		$papeis = $this->registro_model->papeisProjeto();
		$dados = array(
			"opcoes" => $papeis
		);

		$this->load->view("registro/inserir_participante", $dados);

	}


	public function segistro(){

		$this->load->helper("form");

		$this->load->view("registro/pesquisa");

	}

	public function pesquisanumero(){

		$numero = $this->input->post("numero");

		$this->load->model("registro_model");
		$registro = $this->registro_model->pesquisarRegistroNumero($numero);

		if($registro != false){
			$this->load->helper("form");
			$dado = array("registro" => $registro);
			$this->load->view("registro/registro", $dado);

		}else{
			$this->load->helper("form");
			$this->load->view("registro/pesquisa");
		}

	}

	public function pesquisacampo(){

		$this->load->helper("form");

		//codigo aqui
		$titulo = $this->input->post("titulo");
		$data1 = $this->input->post("dt_inicio");
		$data2 = $this->input->post("dt_fim");

		if(!empty($data1)){
			$data1 = explode("/",$data1);
		$data1 = $data1[2].'-'.$data1[1].'-'.$data1[0];

		}
		if(!empty($data2)){

		$data2 = explode("/",$data2);
		$data2 = $data2[2].'-'.$data2[1].'-'.$data2[0];
		}
		$this->load->model("registro_model");
	//	$edital = $this->edital_model->pesquisarEditalNumero($numero);
		$registro = $this->registro_model->pesquisarRegistroAll($titulo, $data1, $data2);

		if(sizeof($registro) != 0){


			$dados = array("registros" => $registro);
			$this->load->view("registro/pesquisa", $dados);
		}

	}

	public function declaracaoregistro($numero = null){
		$dppe = $this->dppe_model->getDppe();

		//$numero = $this->input->post("numero");

		if($numero != null){
			//registro
			$this->load->model("registro_model");
			$registro = $this->registro_model->pesquisarRegistroNumero($numero);

			$edital = array();
			if($registro != false){

				$this->load->model("edital_model");
				$regedital = $this->edital_model->pesquisarEditalId($registro[0]['id_edital']);
				$edano = $this->edital_model->pesquisarEditalAno($regedital[0]['ano']);

				$tratado = array(
					"numero" => $regedital[0]['numero'],
					"ano" => $edano[0]['ano']
				);
				array_push($edital, $tratado);
			}

			//participantes
			$this->load->model("participante_model");
			$participantes = $this->participante_model->buscaParticipantes($numero);

			$pessoas = array();

			foreach($participantes as $participante){
				$dados = $this->participante_model->pesquisarParticipanteNumero($participante->id_participante);

				$tratado = array(
					"papel" => $participante->id_papel,
					"matricula" => $dados[0]['matricula'],
					"nome" => $dados[0]['nome']
				);
				array_push($pessoas, $tratado);
			}


			if($registro != false){
				$dado = array(
					"registro" => $registro,
					"participantes" => $pessoas,
					"edital" => $edital,
					"dppe" => $dppe
				);

				$this->load->view("declaracao/projeto", $dado);

			}else{
				$this->load->view("principal");
			}

		}


	}

	public function declaracaoRegistroPdf($numero = null){
		$dppe = $this->dppe_model->getDppe();

		//$numero = $this->input->post("numero");

		if($numero != null){
			//registro
			$this->load->model("registro_model");
			$registro = $this->registro_model->pesquisarRegistroNumero($numero);

			$edital = array();
			if($registro != false){

				$this->load->model("edital_model");
				$regedital = $this->edital_model->pesquisarEditalId($registro[0]['id_edital']);
				$edano = $this->edital_model->pesquisarEditalAno($regedital[0]['ano']);

				$tratado = array(
					"numero" => $regedital[0]['numero'],
					"ano" => $edano[0]['ano']
				);
				array_push($edital, $tratado);
			}

			//participantes
			$this->load->model("participante_model");
			$participantes = $this->participante_model->buscaParticipantes($numero);

			$pessoas = array();

			foreach($participantes as $participante){
				$dados = $this->participante_model->pesquisarParticipanteNumero($participante->id_participante);

				$tratado = array(
					"papel" => $participante->id_papel,
					"matricula" => $dados[0]['matricula'],
					"nome" => $dados[0]['nome']
				);
				array_push($pessoas, $tratado);
			}


			if($registro != false){
				$dado = array(
					"registro" => $registro,
					"participantes" => $pessoas,
					"edital" => $edital,
					"dppe" => $dppe
				);

				$nomeArquivo = $numero.'.pdf';
				//Load the library
			    $this->load->library('html2pdf');

			    //Set folder to save PDF to
			    $this->html2pdf->folder('./assets/pdf/');

			    //Set the filename to save/download as
			    $this->html2pdf->filename($nomeArquivo);

			    //Set the paper defaults
			    $this->html2pdf->paper('a4', 'portrait');

				//Load html view
    			$this->html2pdf->html($this->load->view("declaracao/projetoImpressao", $dado, true));

    			if($this->html2pdf->create('save')) {
			    	//PDF was successfully saved or downloaded
			    	$this->load->helper('download');


			    	$pth = file_get_contents(base_url()."assets/pdf/". $nomeArquivo);
					force_download($nomeArquivo, $pth);
    			}


				//$this->load->view("declaracao/projeto", $dado);

			}else{
				$this->load->view("principal");
			}

		}

	}

	public function editar($id){

		$this->load->model("registro_model");
		$registro = $this->registro_model->pesquisarRegistroNumero($id);
		// echo "<pre>";
		// var_dump($registro);
		// echo "</pre>";

		$_POST['titulo'] = $registro[0]["titulo"];

		$dt_inicio = explode("-",$registro[0]["dt_inicio"]);
		$dt_inicio = $dt_inicio[1].'/'.$dt_inicio[2].'/'.$dt_inicio[0];

		$_POST['dt_inicio'] = $dt_inicio;

		$dt_fim = explode("-",$registro[0]["dt_fim"]);
		$dt_fim = $dt_fim[1].'/'.$dt_fim[2].'/'.$dt_fim[0];

		$_POST['dt_fim'] = $dt_fim;
		$_POST['descricao'] = $registro[0]["descricao"];

		//dado do edital
		$id_edital = $registro[0]["id_edital"];
		$this->load->model("edital_model");
		$edt = $this->edital_model->pesquisarEditalNumero($id_edital);

		//Esta linha serve para permitir que produtos com acentuação no nome sejam aceitos.
		$this->cart->product_name_rules = "'\d\D'";

		$edano = $this->edital_model->pesquisarEditalAno($edt[0]['ano']);

		//echo "adicionado";
		$data = array(
		 	"id" => $edt[0]['id'],
		 	"qty" => $edt[0]['ano'],
		 	"ano" => $edano[0]['ano'],
		 	"price" => 1.0,
		 	"name" => "edital",
		 	"options" => array("objetivos_gerais" => substr($edt[0]['objetivos_gerais'],0, 80))
		);

		//pesquisar participantes do registro
		$participante =  $this->registro_model->pegarParticipantes($id);

		$dados = array(
			"registro" => $id,
			"participantes" => $participante,
			"edital" => $data,
			"projeto" => $this->registro_model->pesquisarRegistroNumero($id)[0]
		);

		$this->load->view("registro/editar", $dados);

	}

	public function removeProfessor($id, $registro){

		if(isset($id)){
			$data = array(
			'rowid'   => $id,
			'qty'     => 0
			);

			$this->cart->update($data);
		}

		redirect(site_url()."/registro/editar/$registro");
	}

	public function removeParticipante($id, $registro){

		if(isset($id)){
			$data = array(
			'rowid'   => $id,
			'qty'     => 0
			);

			$this->cart->update($data);
		}
		redirect(site_url()."/registro/editar/$registro");
	}

	public function atualizar($id){

		$config['upload_path'] = './assets/uploads/';
		$config['upload_path_raw'] = 'assets/uploads/';
		$config['allowed_types'] = 'pdf|doc|docx';
		$config['max_size']	= '20000';

		$this->load->helper("form");
		$this->load->library('upload', $config);
		$this->load->library("form_validation");
		$this->load->model("registro_model");

		$this->form_validation->set_rules('titulo', 'Título', 'required');
		$this->form_validation->set_rules('dt_inicio', 'Data Início', 'required|valid_date');
		$this->form_validation->set_rules('dt_fim', 'Data Final', 'required|valid_date');
		$this->form_validation->set_rules('localizacao_fisica', 'Localização no Arquivo', 'required|valid_date');
		$this->form_validation->set_rules('consideracoes', 'Considerações', 'required|valid_date');
		// if (empty($_FILES['projeto-arquivo']['name'])) {
		// 	$this->form_validation->set_rules('projeto-arquivo', 'Arquivo digital', 'required');
		// }

		if (!$this->upload->do_upload("projeto-arquivo")) {
			$error = array('error' => $this->upload->display_errors());
			$this->session->set_flashdata("error", $error["error"]);
		} else {
			$data = array('upload_data' => $this->upload->data());
		}


		if($this->form_validation->run() == false) {
			$this->editar($id);

		}else{
			$this->load->model("participante_model");

			// tratar o campo data
			$dt_inicio = $this->input->post("dt_inicio");
			$dt_fim = $this->input->post("dt_fim");

			$dt_inicio = explode("/",$dt_inicio);
			$dt_inicio = $dt_inicio[2].'-'.$dt_inicio[0].'-'.$dt_inicio[1];

			$dt_fim = explode("/",$dt_fim);
			$dt_fim = $dt_fim[2].'-'.$dt_fim[0].'-'.$dt_fim[1];

			// dados do registro
			$registro = array(
				"id" => $id,
				"titulo" => $this->input->post("titulo"),
				"descricao" => $this->input->post("descricao"),
				"dt_inicio" => $dt_inicio,
				"dt_fim" => $dt_fim,
				"localizacao_fisica" => $this->input->post("localizacao_fisica"),
				"consideracoes" => $this->input->post("consideracoes")
			);

			// Se houver upload, salva no banco
			if (isset($data)) {
				$registro["arquivo_url"] = $config['upload_path_raw'].$data["upload_data"]["file_name"];
			}

			//salvar registro
			$this->registro_model->atualizarRegistro($registro);

				//pegar dados da sessão - Professor
				$arrayProfessor = array();
				foreach ($this->cart->contents() as $items){
					if($items['name'] == "professor"){
						$siapeProfessor = $items['id'];
						// array_push($arrayProfessor, $siapeProfessor);

						//verifica se existe um professor no cadatro de participante
						$id_professor = $this->participante_model->pegaId($siapeProfessor);
						//se não existir cadastra
						if(empty($id_professor)){
							$this->load->model("integrante_model");
							$integrante = $this->integrante_model->pesquisarIntegranteProfessorMatricula($siapeProfessor);

							//array com os dados que devem ser salvos
							$dadosProfessor = array(
								"nome" => $integrante[0]['nome'],
								"matricula" => $integrante[0]['siape'],
								"curso" => $integrante[0]['instituicao'],
								"situacao_matricula" => $integrante[0]['situacao'],
								"tipo" => "PROFESSOR"
							);
							$id_professor = $this->participante_model->salvarParticipante($dadosProfessor);
						}else{
							//pega o id do participante existente
							$id_professor = $id_professor[0]['id'];
						}

						$porfessor_dt_inicio = $items['options']['data'];
						$dt_inicio = explode("/",$porfessor_dt_inicio);
						$dt_inicio = $dt_inicio[2].'-'.$dt_inicio[0].'-'.$dt_inicio[1];

						$this->load->model("ajax_model");
						$id_papel = $this->ajax_model->getIdPapel($items['options']['papel']);

						$dados = array(
							"id_registro" => $id,
							"id_participante" => $id_professor,
							"id_papel" => $id_papel[0]->id,
							"dt_inicio" => $dt_inicio,
							"ativo" => "S"
						);
						$resultadoFinal = $this->registro_model->salvarRegistroParticipante($dados);
					}
				}

				//pegar dados da sessão - Participante
				$arrayParticipante = array();
				foreach ($this->cart->contents() as $items){
					if($items['name'] == "participante"){
						$matriculaParticipante = $items['id'];

						//verifica se existe um participante no cadatro
						$id_participante = $this->participante_model->pegaId($matriculaParticipante);
						//se não existir cadastra
						if(empty($id_participante)){
							$this->load->model("integrante_model");
							$integrante = $this->integrante_model->pesquisarIntegranteAlunoMatricula($matriculaParticipante);

							//array com os dados que devem ser salvos
							$dadosParticipante = array(
								"nome" => $integrante[0]['nome'],
								"matricula" => $integrante[0]['matricula'],
								"curso" => $integrante[0]['curso'],
								"situacao_matricula" => $integrante[0]['situacao_matricula'],
								"tipo" => "ALUNO"
							);
							$id_participante = $this->participante_model->salvarParticipante($dadosParticipante);
						}else{
							//pega o id do participante existente
							$id_participante = $id_participante[0]['id'];
						}

						$participante_dt_inicio = $items['options']['data'];
						$dt_inicio = explode("/",$participante_dt_inicio);
						$dt_inicio = $dt_inicio[2].'-'.$dt_inicio[0].'-'.$dt_inicio[1];

						$this->load->model("ajax_model");
						$id_papel = $this->ajax_model->getIdPapel($items['options']['papel']);

						$dados = array(
							"id_registro" => $id,
							"id_participante" => $id_participante,
							"id_papel" => $id_papel[0]->id,
							"dt_inicio" => $dt_inicio,
							"ativo" => "S"
						);
						$resultadoFinal = $this->registro_model->salvarRegistroParticipante($dados);
					}
				}

				$this->cart->destroy();
				$this->session->set_flashdata("sucesso", "Registro Atualizado com Sucesso");
				redirect('registro/mostra/'.$id);
		}
	}

	public function fecharParticipacao($registro, $participante){
		//encerrar participação
		$data = date('Y-m-d', time());
		$dado = array(
			"id" => $participante,
			"dt_fim" => $data
		);
		$this->load->model("registro_model");
		$this->registro_model->fechaParticipacao($dado);
		$this->editar($registro);
	}
}
