<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Participante extends CI_Controller {

	/**
	* Layout default utilizado pelo controlador.
	*/
	public $layout = 'principal';
	 
	/**
	* Titulo default.
	*/
	public $title = 'DPPE Online - Participante';
	 
	/**
	* Definindo os css default.
	*/
	public $css = array('bootstrap-theme.min', 'bootstrap.min', 'styletablehand', 'jquery-ui', 'jquery-ui.min');
	 
	/**
	* Carregando os js default.
	*/
	public $js = array('jquery.min', 'bootstrap.min',  'jquery-ui', 'jquery-ui.min');



	public function __construct(){
		parent::__construct();
		if(!$this->session->userdata('session_id') || !$this->session->userdata('logado')){
			redirect(site_url('login'));
		}
		
		$this->load->library('grocery_CRUD');	
		$this->load->model('dppe_model');
	}

	public function _dppe_output($output = null){
		$this->load->view('exibir',$output);
	}


	public function index(){
		$this->participante_gerenciamento();
		//$this->_dppe_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
		// $this->load->view('exibir.php',$output);
	}



	public function participante_gerenciamento(){
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('participantes');
			$crud->set_subject('Participante');
			$crud->required_fields('nome','matricula','cpf', 'org_emissor', 'org_emissor_uf', 'dt_nascimento','identidade');
			$crud->columns('nome','matricula','tipo');

			$crud->display_as('org_emissor','Órgão Emissor')
				 ->display_as('org_emissor_uf','Órgão Emissor - UF')
				 ->display_as('numero','Número')
				 ->display_as('agencia','Agência')
				 ->display_as('operacao','Operação')
				 ->display_as('dt_nascimento','Data Nascimento')
				 ->display_as('cpf','CPF')
				 ->display_as('situacao_matricula','Situação da Matrícula')
				 ->display_as('matricula','Matrícula');		

			$crud->add_fields('nome','cpf', 'identidade','org_emissor', 'org_emissor_uf', 'dt_nascimento', 'tipo',
				'curso', 'matricula', 'situacao_matricula', 'logradouro','numero','complemento','cep','bairro','cidade','estado',
				'banco', 'conta', 'agencia','operacao');	

			$crud->edit_fields('nome','cpf', 'identidade','org_emissor', 'org_emissor_uf', 'dt_nascimento', 'tipo',
				'curso', 'matricula', 'situacao_matricula', 'logradouro','numero','complemento','cep','bairro','cidade','estado',
				'banco', 'conta', 'agencia','operacao');
					

			//botão de declaração
			
			$crud->add_action('Visualizar', '', 'participante/visualizarparticipante','ui-icon-document');
			$crud->add_action('Declaração', '', 'participante/declaracaoparticipante','ui-icon-clipboard');

			$crud->unset_export();
			$crud->unset_print();
			$crud->unset_delete();
			$crud->unset_read();

			$output = $crud->render();

			$this->_dppe_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}	


	public function listarParticipante(){
		

		$this->load->model("participante_model");
		$participante = $this->participante_model->buscarTodosRegistros();	
		
		$dados = array("participantes" => $participante);
		
		$this->load->view("participante/participantes", $dados);		
	}

	public function visualizarparticipante($numero = null){
		//codigo aqui
			
		//$numero = $this->input->post("numero");
						
		if($numero != null){
			//participantes
			$this->load->model("participante_model");
			$participantes = $this->participante_model->pesquisarParticipanteNumero($numero);

			//registro
			$this->load->model("registro_model");
			$registros = $this->registro_model->buscaRegistro($numero);

			$valores = array();
		
			foreach($registros as $registo){
				$dados = $this->registro_model->pesquisarRegistroNumero($registo->id_registro);
				$datafim = $registo->dt_fim;
				if($datafim == null){
					$datafim = "--";
				}

				$this->load->model("edital_model");
				$regedital = $this->edital_model->pesquisarEditalId($dados[0]['id_edital']);
				$edano = $this->edital_model->pesquisarEditalAno($regedital[0]['ano']);

				$tratado = array(
					"id" => $registo->id_registro,
					"papel" => $registo->id_papel,
					"titulo" => $dados[0]['titulo'],
					"dt_inicio" => $registo->dt_inicio,
					"dt_fim" => $datafim,
					"id_edital" => $regedital[0]['id'],
					"numeroedital" => $regedital[0]['numero'],
					"anoedital" => $edano[0]['ano']
				);
				array_push($valores, $tratado);
								
			}
					
			if($participantes != false){				
				$dado = array(
					"registro" => $valores,
					"participante" => $participantes
				);

				$this->load->view("participante/participante", $dado);

		}else{
			$this->load->view("principal");
		}
		
		}			
	
	}

	public function declaracaoparticipante($numero = null){
		$dppe = $this->dppe_model->getDppe();
			
		//$numero = $this->input->post("numero");
						
		if($numero != null){
			//participantes
			$this->load->model("participante_model");
			$participantes = $this->participante_model->pesquisarParticipanteNumero($numero);

			//registro
			$this->load->model("registro_model");
			$registros = $this->registro_model->buscaRegistro($numero);

			$valores = array();
		
			foreach($registros as $registo){
				$dados = $this->registro_model->pesquisarRegistroNumero($registo->id_registro);
				$datafim = $registo->dt_fim;
				if($datafim == null){
					$datafim = "--";
				}

				$this->load->model("edital_model");
				$regedital = $this->edital_model->pesquisarEditalId($dados[0]['id_edital']);
				$edano = $this->edital_model->pesquisarEditalAno($regedital[0]['ano']);

				$tratado = array(
					"papel" => $registo->id_papel,
					"titulo" => $dados[0]['titulo'],
					"dt_inicio" => $registo->dt_inicio,
					"dt_fim" => $datafim,
					"numeroedital" => $regedital[0]['numero'],
					"anoedital" => $edano[0]['ano']
				);
				array_push($valores, $tratado);
				
			}
					
			if($participantes != false){				
				$dado = array(
					"registro" => $valores,
					"participante" => $participantes,
					"dppe" => $dppe
				);

				$this->load->view("declaracao/participante", $dado);
		
			}else{				
				$this->load->view("principal");
			}
		
		}			
	
	}

	public function declaracaoParticipantePdf($numero = null){
		$dppe = $this->dppe_model->getDppe();
						
		if($numero != null){
			//participantes
			$this->load->model("participante_model");
			$participantes = $this->participante_model->pesquisarParticipanteNumero($numero);

			//registro
			$this->load->model("registro_model");
			$registros = $this->registro_model->buscaRegistro($numero);

			$valores = array();

			foreach($registros as $registo){
				$dados = $this->registro_model->pesquisarRegistroNumero($registo->id_registro);
				$datafim = $registo->dt_fim;
				if($datafim == null){
					$datafim = "--";
				}

				$this->load->model("edital_model");
				$regedital = $this->edital_model->pesquisarEditalId($dados[0]['id_edital']);
				$edano = $this->edital_model->pesquisarEditalAno($regedital[0]['ano']);

				$tratado = array(
					"papel" => $registo->id_papel,
					"titulo" => $dados[0]['titulo'],
					"dt_inicio" => $registo->dt_inicio,
					"dt_fim" => $datafim,
					"numeroedital" => $regedital[0]['numero'],
					"anoedital" => $edano[0]['ano']
				);
				array_push($valores, $tratado);
			}
					
			if($participantes != false){				
				$dado = array(
					"registro" => $valores,
					"participante" => $participantes,
					"dppe" => $dppe
				);

				

				$nomeArquivo = $numero.'.pdf';
				//Load the library
			    $this->load->library('html2pdf');
			    
			    //Set folder to save PDF to
			    $this->html2pdf->folder('./assets/pdf/');

			    //Set the filename to save/download as
			    $this->html2pdf->filename($nomeArquivo);
			    
			    //Set the paper defaults
			    $this->html2pdf->paper('a4', 'portrait');

				//Load html view	    			
    			$this->html2pdf->html($this->load->view("declaracao/participanteImpressao", $dado, true));

    			if($this->html2pdf->create('save')) {
			    	//PDF was successfully saved or downloaded
			    	$this->load->helper('download');

			    	
			    	$pth    =   file_get_contents(base_url()."assets/pdf/". $nomeArquivo);
					force_download($nomeArquivo, $pth);   
    			}
			}
		}
	}


}