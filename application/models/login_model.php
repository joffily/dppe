<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Login_model extends CI_Model{
	
	public function verifica($log){

		//verificando se existe uma pessoa pelo nome já cadastrado
		$this->db->where('login', $log['login']);
		$this->db->where('senha', $log['senha']);
		$this->db->where('ativo', 1);
		
		$usuario = $this->db->get('usuarios')->result();
		
		if(count($usuario) === 1){
			return true; //usuário existe
		}else{
			return false;
		}
		
	}	

	public function getUsuario($login){
		$this->db->where('login', trim($login));
		return $this->db->get("usuarios")->row();			
	}

}
