<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Edital_model extends CI_Model{
	
	
	public function salvarEdital($edital){
		
		$this->db->where('numero',$edital['numero']);
		$this->db->where('ano', $edital['ano']);
		$this->db->get('edital');
		$total = $this->db->affected_rows();
		
		if($total == 0){
			//insert do registro
			$this->db->insert("edital", $edital);
			return true;
		}else{
			return false;
		}
		
	}
	
	public function editandoEdital($edita, $id){
		$this->db->where('id', $id);
		$resultado = $this->db->get("edital")->result_array();		

		if(count($resultado) == 1){
		
			$this->db->update("edital", $edita, array('id' => $id));
			return true;
		}else{
			return false;
		}		
		
	}

	public function buscarTodosEditais(){
				
		return $this->db->get("edital")->result_array();
		
	}

	public function pesquisarEditalId($numero){
		$this->db->where('id', $numero);
		$resultado = $this->db->get("edital")->result_array();		

		if(count($resultado) == 1){
			return $resultado;
		}else{
			return false;
		}				
	}

public function pesquisarEditalAno($numero){
		$this->db->where('id', $numero);
		$resultado = $this->db->get("ano_edital")->result_array();		

		if(count($resultado) == 1){
			return $resultado;
		}else{
			return false;
		}				
}

public function pesquisarEditalTipo($numero){
		$this->db->where('id', $numero);
		$resultado = $this->db->get("tipo_edital")->result_array();		

		if(count($resultado) == 1){
			return $resultado;
		}else{
			return false;
		}				
}

public function pesquisarEditalPrograma($numero){
		$this->db->where('id', $numero);
		$resultado = $this->db->get("programa_financiador")->result_array();		

		if(count($resultado) == 1){
			return $resultado;
		}else{
			return false;
		}				
}

public function pesquisarEditalFRecurso($numero){
		$this->db->where('id', $numero);
		$resultado = $this->db->get("financiadora_recurso")->result_array();		

		if(count($resultado) == 1){
			return $resultado;
		}else{
			return false;
		}				
}

public function pesquisarEditalNumero($edital){
		$this->db->where('id', trim($edital));
		return $this->db->get("edital")->result_array();	
				
	}
	
	public function pesquisarEditalData($numero, $data1, $data2){
	
		if(!empty($numero)){
			$this->db->where('numero', $numero);
		}

		if(!empty($data1) && !empty($data2)){
			$this->db->where("dt_publicacao BETWEEN '{$data1}' AND '{$data2}'");
		}

		$resultado= $this->db->get("edital")->result_array();
				
		if(count($resultado) != 0){
			return $resultado;
		}else{
			return false;
		}				
	}


}