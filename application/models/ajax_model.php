<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax_model extends CI_Model{
	
	//limitado a 10 registro por pesquisa
	
	public function getAlunosMatricula($matricula){
		if(isset($matricula)){
			$this->db->like('matricula', $matricula);	
		}		
		$this->db->limit(10);
		return $this->db->get('alunos')->result();
		
	}

	public function getAlunosNome($nome){
		if(isset($nome)){
			$this->db->like('nome', $nome);
		}		
		$this->db->limit(10);
		return $this->db->get('alunos')->result();
	}
	
	public function getEditalNumero($numero){
		if(isset($numero)){
			$this->db->like('numero', $numero);
		}		
		$this->db->limit(5);
		return $this->db->get('edital')->result();
	}

	public function getEditalObjetivo($texto){
		if(isset($texto)){
			$this->db->like('objetivos_gerais', $texto);
		}		
		$this->db->limit(5);
		return $this->db->get('edital')->result();
	}

	public function getProfessorMatricula($matricula){
		if(isset($matricula)){
			$this->db->like('siape', $matricula);	
		}		
		$this->db->limit(10);
		return $this->db->get('professores')->result();
		
	}

	public function getProfessorNome($nome){
		if(isset($nome)){
			$this->db->like('nome', $nome);
		}		
		$this->db->limit(10);
		return $this->db->get('professores')->result();
	}

	public function getUmProfessor($matricula){
		
		if($matricula == null){
			return false;
		}
		$this->db->from('professores');
		$this->db->where('siape', $matricula);

		$total = $this->db->get()->num_rows();

		if($total == 0){
			return false;
		}else{
			$this->db->where('siape', $matricula);
			return $this->db->get('professores')->result();
		}		
	}

	public function getUmAluno($matricula){
		
		if($matricula == null){
			return false;
		}
		$this->db->from('alunos');
		$this->db->where('matricula', $matricula);

		$total = $this->db->get()->num_rows();

		if($total == 0){
			return false;
		}else{
			$this->db->where('matricula', $matricula);
			return $this->db->get('alunos')->result();
		}		
	}

	public function getPapel($papel){
		$this->db->where("id", $papel);
		return $this->db->get('papeis')->result();
	}

	public function getIdPapel($papel){
		$this->db->where("descricao", $papel);
		return $this->db->get('papeis')->result();
	}
}