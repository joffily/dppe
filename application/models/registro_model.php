<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registro_model extends CI_Model{
	
	
	public function buscarTodosRegistros(){
				
		return $this->db->get("registros")->result_array();
		
	}
	
	public function pesquisarRegistroNumero($id){
		$this->db->where('id', $id);
		$resultado = $this->db->get("registros")->result_array();		

		if(count($resultado) == 1){
			return $resultado;
		}else{
			return false;
		}		
		
	}
	
	public function pesquisarRegistroAll($titulo, $data1, $data2){
	
		if(!empty($titulo)){
			$this->db->like('titulo', $titulo);
		}

		if(!empty($data1)){
			$this->db->where('dt_inicio', $data1);
		}
		if(!empty($data2)){
			$this->db->where('dt_fim', $data2);
		}
	
		$resultado= $this->db->get("registros")->result_array();		

		if(count($resultado) != 0){
			return $resultado;
		}else{
			return false;
		}		
		
	}
	
	public function pesquisarRegistroNumero1($cod){
		$this->db->where('codigo', $cod);
		$resultado = $this->db->get("registros")->result();
	
		if(count($resultado) == 1){
			return $resultado;
		}else{
			return false;
		}
	
	
	}	

	public function pesquisarRegistroCodigo($titulo){
		$this->db->where('titulo', $titulo);
		$resultado = $this->db->get("registros")->result_array();
	
		if(count($resultado) == 1){
			return $resultado;
		}else{
			return false;
		}	
	
	}

	public function pesquisarRegistroEdital($edital){
		$this->db->where('id_edital', $edital);
		return $this->db->get("registros")->result();

	}

	public function pesquisarRegistroParticipantes($reg){
		$this->db->where('id_registro', $reg);
		$esultado = $this->db->get("registro_participante")->result_array();
		

		if(count($resultado) == 1){
			return $resultado;
		}else{
			return false;
		}	
	
	}


	public function salvarRegistro($registro){
		//verificando se existe um Registro com o mesmo titulo e dt_inicio
		$this->db->where('dt_inicio',$registro['dt_inicio']);
		$this->db->where('titulo', $registro['titulo']);
		$this->db->get('registros');
		$total = $this->db->affected_rows();
		
		if($total == 0){
			//insert do registro
			$this->db->insert("registros", $registro);
			return true;
		}else{
			return false;
		}
		
	}

	public function salvarRegistroReturn($registro){
		//verificando se existe um Registro com o mesmo titulo e dt_inicio
		$this->db->where('dt_inicio',$registro['dt_inicio']);
		$this->db->where('titulo', $registro['titulo']);
		$this->db->get('registros');
		$total = $this->db->affected_rows();
		
		if($total == 0){
			//insert do registro
			$this->db->insert("registros", $registro);
			return $this->db->insert_id();
		}else{
			return false;
		}
		
	}

	public function papeisProjeto(){
		$dados = $this->db->get("papeis")->result();	
		return $dados;
	}

	public function papeisNome($id){
		$this->db->where("id", $id);
		$dados = $this->db->get("papeis")->result();	
		return $dados;
	}

	public function papeisProjetoProfessor(){
		//professores não podem ser bolsista nem servidor
		$papel = array('Bolsista','Servidor');
		$this->db->where_not_in('descricao', $papel);
		return $this->db->get("papeis")->result();			
	}

	public function papeisProjetoParticipante(){
		//professores não podem ser bolsista nem servidor
		$papel = array('Bolsista','Servidor','Voluntário');
		$this->db->where_in('descricao', $papel);
		return $this->db->get("papeis")->result();			
	}

	public function salvarRegistroParticipante($dados){

		//verificando se existe um Registro com o mesmo titulo e numero
		$this->db->where('id_registro',$dados['id_registro']);
		$this->db->where('id_participante', $dados['id_participante']);
		$this->db->get('registro_participante');
		$total = $this->db->affected_rows();
		
		if($total == 0){
			//insert
			$this->db->insert("registro_participante", $dados);
			return true;
		}else{
			return false;
		}
	}

	public function buscaRegistro($id_participante){
		$this->db->where('id_participante', $id_participante);
		return $this->db->get("registro_participante")->result();
	}

	public function pegarParticipantes($id){
		$sql = "select p.matricula, p.nome, p.tipo, rp.dt_inicio, rp.dt_fim, rp.id_papel, rp.id";
		$sql .= " from registros r left join registro_participante rp on (r.id = rp.id_registro)";
		$sql .= " left join participantes p on (rp.id_participante = p.id) where r.id = " . $id;		
		$query = $this->db->query($sql);
		
		return $query->result();
	}

	public function pegarPart($id){
		$sql = "select p.matricula, p.nome, p.tipo, rp.dt_inicio, rp.dt_fim, rp.id_papel, p.id";
		$sql .= " from registros r left join registro_participante rp on (r.id = rp.id_registro)";
		$sql .= " left join participantes p on (rp.id_participante = p.id) where r.id = " . $id;		
		$query = $this->db->query($sql);
		
		return $query->result();
	}

	public function atualizarRegistro($dado){
		$this->db->where("id", $dado['id']);
		$resultado = $this->db->update("registros", $dado);
		return $resultado;

	}

	public function fechaParticipacao($dado){
		$this->db->where("id", $dado['id']);
		$this->db->update("registro_participante", $dado);
	}

}