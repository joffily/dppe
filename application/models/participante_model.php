<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Participante_model extends CI_Model{

	public function salvarParticipante($participante){

		//verificando se existe uma matricula cadastrada
		// $this->db->where('matricula',$participante['matricula']);
		
		// $this->db->get('participantes');
		// $total = $this->db->affected_rows();
		$total = 0;
		
		if($total == 0){
			//insert do registro
			$this->db->insert("participantes", $participante);
			return $this->db->insert_id();
		}else{
			return false;
		}
	}

	public function salvarParticipante1($participante){

		//insert do registro
		$this->db->insert("participantes", $participante);
		return $this->db->insert_id();
	
	}

	public function salvarEndereco($endereco){

		$this->db->insert("enderecos", $endereco);
		return true;		

	}


	public function salvarConta($conta){

		$this->db->insert("contas", $conta);
		return true;		

	}

	public function buscarTodosRegistros(){

		return $this->db->get("participantes")->result_array();

	}

	public function pesquisarParticipanteNumero($numero){
		$this->db->where('id', $numero);
		$resultado = $this->db->get("participantes")->result_array();		

		if(count($resultado) == 1){
			return $resultado;
		}else{
			return false;
		}		
		
	}	

	public function pesquisarParticipanteNome($nome){
		$this->db->where('nome', $nome);
		$resultado = $this->db->get("participantes")->result_array();		

		if(count($resultado) == 1){
			return $resultado;
		}else{
			return false;
		}		
		
	}	

	// public function pesquisarParticipanteNumero1($numero){
	// 	$this->db->where('id', $numero);
	// 	$resultado = $this->db->get("participantes")->result();		

	// 	if(count($resultado) == 1){
	// 		return $resultado;
	// 	}else{
	// 		return false;
	// 	}		
		
	// }

	public function pesquisarParticipanteEndereco($numero){
		$this->db->where('id_participante', $numero);
		$resultado = $this->db->get("enderecos")->result_array();		

		if(count($resultado) == 1){
			return $resultado;
		}else{
			return false;
		}		
	}

	public function pesquisarParticipanteConta($numero){
		$this->db->where('id_participante', $numero);
		$resultado = $this->db->get("contas")->result_array();		

		if(count($resultado) == 1){
			return $resultado;
		}else{
			return false;
		}		
	}


	public function pesquisarParticipanteMatricula($numero){
		$this->db->where('matricula', trim($numero));
		$resultado = $this->db->get("participantes")->result_array();		

		if(count($resultado) == 1){
			return $resultado;
		}else{
			return false;
		}		
		
	}	


	public function pesquisarParticipanteMat($numero){
		

		if(!empty($numero)){
			$this->db->where('matricula', trim($numero));
		}

		$resultado = $this->db->get("participantes")->result_array();		

		if(count($resultado) != 0){
			return $resultado;
		}else{
			return false;
		}		
		
	}	

	public function pegaId($matricula){

		$this->db->where('matricula', $matricula);
		return $this->db->get("participantes")->result_array();
	}

	public function buscaParticipantes($id_registro){

		$this->db->where('id_registro', $id_registro);
		return $this->db->get("registro_participante")->result();
	}

}
