<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pesquisa_model extends CI_Model{
	
	public function curso(){
		$this->db->distinct();
		$this->db->select("curso");
		$this->db->order_by("curso", "desc");
		return $this->db->get("alunos")->result_array();			
	}

	public function matricula(){
		$this->db->distinct();
		$this->db->select("situacao_matricula");
		return $this->db->get("alunos")->result_array();			
	}

	public function titularidade(){
		$this->db->distinct();
		$this->db->select("titularidade");
		return $this->db->get("professores")->result_array();			
	}

	public function situacaoProfessor(){
		$this->db->distinct();
		$this->db->select("situacao");
		return $this->db->get("professores")->result_array();			
	}

	public function instituicaoProfessor(){
		$this->db->distinct();
		$this->db->select("instituicao");
		return $this->db->get("professores")->result_array();			
	}

	public function pesquisaIdAluno($id){
		
		$this->db->where('id', $id);
		$resultado = $this->db->get("alunos")->result_array();		

		if(count($resultado) == 1){
			return $resultado;
		}else{
			return false;
		}				
		
	}
	public function pesquisaIdProfessor($id){
		
		$this->db->where('id', $id);
		$resultado = $this->db->get("professores")->result_array();		

		if(count($resultado) == 1){
			return $resultado;
		}else{
			return false;
		}				
		
	}
	public function pesquisaNomeAluno($nome, $situacao_matricula, $curso){

        if(!empty($nome)){
            $this->db->like('nome', $nome,'after');
        }else{
            //se não especificar o nome só exibe os 1000 primeiros registro
            $this->db->limit(1000);
        }

        // if(!empty($situacao_matricula !== '0')){
        if(!empty($situacao_matricula) && $situacao_matricula != '0'){
            $this->db->like('situacao_matricula', $situacao_matricula);
        }
        

        //if(!empty($curso != '0')){
        if(!empty($curso) && $curso != '0'){
            $this->db->like('curso', $curso);
        }
        


        return $this->db->get("alunos")->result_array();
    }

	public function pesquisaMatriculaAluno($matricula){

		$this->db->where('matricula', $matricula);
		return $this->db->get("alunos")->result_array();

	}

	public function pesquisaNomeProfessor($nome, $titularidade, $situacao, $instituicao){

		if(!empty($nome)){
			$this->db->like('nome', $nome,'after');
		}

		
		// if(!empty($titularidade != '0')){
		if(!empty($titularidade) && $titularidade != '0'){	
			$this->db->like('titularidade', $titularidade);	
		}

		// if(!empty($situacao != '0')){
		if(!empty($situacao) && $situacao != '0'){
			$this->db->like('situacao', $situacao);	
		}
		
		// if(!empty($instituicao != '0')){
		if(!empty($instituicao) && $instituicao != '0'){	
			$this->db->like('instituicao', $instituicao);	
		}

		return $this->db->get("professores")->result_array();
	}

}