<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Declaracao_model extends CI_Model{

  public function create($declaracao){

    //insert do registro
    $this->db->insert("declaracoes", $declaracao);
    return $this->db->insert_id();
  }

  public function get_by_hash($hash){
    $this->db->where('hash', $hash);
    return $this->db->get("declaracoes")->result_array();
  }

}
