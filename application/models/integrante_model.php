<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Integrante_model extends CI_Model{
		
	
	public function pesquisarIntegranteAlunoMatricula($numero){
		$this->db->where('matricula', trim($numero));
		return $this->db->get("alunos")->result_array();			
	}

	public function pesquisarIntegranteProfessorMatricula($numero){
		$this->db->where('siape', trim($numero));
		return $this->db->get("professores")->result_array();			
	}


}