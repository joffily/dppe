<!doctype html>
<html lang="pt-BR">
<head>
  <meta charset="utf-8">
  <title>{TITULO_PAGINA}</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Projeto DPPE online">
  <meta name="author" content="Valéria Cavalcanti & Natacha Targino & Jaqueline Fernandes">
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"> -->
  {CSS_LAYOUT}
  {JS_LAYOUT}

  <?php if(isset($css_files)):?>
    <?php foreach($css_files as $file): ?>
      <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>

    <?php foreach($js_files as $file): ?>
      <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>
  <?php endif; ?>

  <!-- css para corrigir os inputs do plugin -->
  <style type="text/css">

  input {
  }
  #ui-datepicker-div{
    width: 250px !important;
  }

  * { box-sizing: border-box; }
  html, body {
    width: 100%;
    height: 100%;
    margin:0;
    padding:0;
    font-family: sans-serif;
  }

  .form-control {
    width: 100%;
    border: 1px solid #ddd;
    padding: 10px;
    border-radius: 3px;
  }
  </style>

  <style media="print">
    .print {
      display: none;
    }
  </style>

</head>

<body>
  <div style=" width:100%; height:100%;  background:  #FBFDFA;">
    <div class="container" style=" min-height: 100%;background-color:#fafbfa; -moz-box-shadow: 0 0 2px 2px #888; -webkit-box-shadow: 0 0 2px 2px #888; box-shadow: 0 0 2px 2px #888; -moz-border-radius:5px;
    -webkit-border-radius:5px; border-radius:5px; position: relative;">
    <div class="print" style="border-bottom: 1px solid #D1D1D1;">
    <img style=" width:100%;height:160px;" src="<?= base_url("assets/img/logosite.jpg"); ?>" />
    </div>

    <div style="width:80%; top:0;bottom: 0;left: 0;right: 0;margin: auto; position: relative;">
      <!-- conteudo -->
      <div>
        <?php if($this->session->flashdata('erro')): ?>
          <h4 class='alert alert-danger' align="center"><?php echo $this->session->flashdata('erro'); ?> </h4>
        <?php elseif ($this->session->flashdata('sucesso')): ?>
          <h4 class='alert alert-success' align="center"><?php echo $this->session->flashdata('sucesso'); ?> </h4>
        <?php endif ?>

        {CONTEUDO}
      </div>
      <!-- Fim do conteúdo -->
    </div>

    <br><br><br><br><br><br><br><br>

    <div class="panel-footer" style=" bottom: 0; width:100%; margin:0; position: absolute;"><br>
      <p style='text-align: center;'><strong>Instituto Federal de Educação, Ciência e Tecnologia da Paraíba</strong></p>
      <p style='text-align: center;'>Av. Primeiro de Maio, 720 - Jaguaribe, João Pessoa - PB, 58015-435</p>
    </div>


  </div>
</div>
</body>
</html>
