<!DOCTYPE html>
<html lang="pt-BR">
<head>
  <meta charset="utf-8">
  <title>{TITULO_PAGINA}</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="DPPE online - Campus João Pessoa">
  <meta name="author" content="Valéria Cavalcanti & Natacha Targino & Jaqueline Fernandes">    
    {CSS_LAYOUT}
    {JS_LAYOUT}
</head>

<body>

    {CONTEUDO}       

    
<div id="rodape">
     <div class="panel-footer"><br>
		<p>© DPPE online, Todos os direitos reservados.</p>
	  </div>
</div>
</body>
</html>
