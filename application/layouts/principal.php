<!doctype html>
<html lang="pt-BR">
<head>
  <meta charset="utf-8">
  <title>{TITULO_PAGINA}</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Projeto DPPE online">
  <meta name="author" content="Valéria Cavalcanti & Natacha Targino & Jaqueline Fernandes">

    {CSS_LAYOUT}
    {JS_LAYOUT}

<?php if(isset($css_files)):?>
            <?php foreach($css_files as $file): ?>
                <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
            <?php endforeach; ?>

            <?php foreach($js_files as $file): ?>
                <script src="<?php echo $file; ?>"></script>
            <?php endforeach; ?>    
<?php endif; ?>

    <!-- css para corrigir os inputs do plugin -->
    <style type="text/css">
        
        input {
            height: 30px !important;
        }
        #ui-datepicker-div{ 
            width: 250px !important;
        }

    html, body {
      width: 100%;
      height: 100%;
      margin:0;
      padding:0;
    }
    </style>

</head>

<body>
<div style=" width:100%; height:100%;  background:  #FBFDFA;">
<div class="container" style=" min-height: 100%;background-color:#fafbfa; -moz-box-shadow: 0 0 2px 2px #888; -webkit-box-shadow: 0 0 2px 2px #888; box-shadow: 0 0 2px 2px #888; -moz-border-radius:5px;
-webkit-border-radius:5px; border-radius:5px; position: relative;">
  <div style="border-bottom: 1px solid #D1D1D1;">
  <a href=<?=site_url('principal')?> ><img style=" width:100%;height:160px;" src="<?= base_url("assets/img/logosite.jpg"); ?>" /></a>
  </div>
    <div class="row clearfix">
        <div class="col-md-12 column">
           <?php if($this->session->userdata('logado')): ?>              
            <nav class="navbar navbar-default" role="navigation">    
                <div class="navbar-header">
                     <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> 
                     <span class="sr-only"></span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                     </button> 
                </div>          
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                             <a href=<?=site_url('principal')?> >Home</a>
                            
                        </li>
                    </ul> 
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href=<?= site_url("edital")?> >Edital</a>
                        </li>
                    </ul>                   
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                             <a href="<?= site_url("registro/listarregistro")?>"><i class="fa fa-angle-double-right"></i>Registro</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href=<?php echo site_url("participante")?>>Participante</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown">Consulta<strong class="caret"></strong></a>
                            <ul class="dropdown-menu">
                                <li>    
                                    <a href="<?php echo site_url('pesquisa/aluno');?>">Pesquisar Por Aluno</a>
                                    <a href="<?php echo site_url('pesquisa/professor');?>">Pesquisar Por Professor</a>
                                </li>  
                            </ul>
                        </li>
                    </ul>   
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                             <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema<strong class="caret"></strong></a>
                            <ul class="dropdown-menu">
                                <li>    
                                    <a href="<?php echo site_url('sistema/usuario_gerenciamento');?>">Gerenciar Usuários</a>
                                    <a href="<?php echo site_url('sistema/dados_dppe_gerenciamento');?>">Gerenciar Dados do DPPE</a>
                                    <a href="<?php echo site_url('sistema/financiadora_recurso_gerenciamento');?>">Gerenciar Financiadora de Recurso</a>
                                    <a href="<?php echo site_url('sistema/programa_financiador_gerenciamento');?>">Gerenciar Programa Financiador</a>
                                    <a href="<?php echo site_url('sistema/papel_gerenciamento');?>">Gerenciar Papéis</a>
                                    <a href="<?php echo site_url('sistema/tipo_edital_gerenciamento');?>">Gerenciar Tipo de Edital</a>
                                    <a href="<?php echo site_url('sistema/ano_edital_gerenciamento');?>">Gerenciar Ano do Edital</a>
                                </li>  
                            </ul>
                        </li>
                    </ul>  
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="<?php echo site_url('login/logout')?>">Sair</a>
                        </li>
                        
                    </ul>
                </div>
                
            </nav>
            <?php endif ?>        
        </div>
    </div>
           
    <div style=" width:80%; top:0;bottom: 0;left: 0;right: 0;margin: auto; position: relative;">
        <!-- conteudo -->            
         <div>
            <?php if($this->session->flashdata('erro')): ?> 
                <h4 class='alert alert-danger' align="center"><?php echo $this->session->flashdata('erro'); ?> </h4>
            <?php elseif ($this->session->flashdata('sucesso')): ?>
                <h4 class='alert alert-success' align="center"><?php echo $this->session->flashdata('sucesso'); ?> </h4>
            <?php endif ?>
            
             {CONTEUDO}             
        </div>  
    <!-- Fim do conteúdo -->
    </div> 

    <br><br><br><br><br><br><br><br>

    <div class="panel-footer" style=" bottom: 0; width:100%; margin:0; position: absolute;"><br>
       <p style='text-align: center;'><strong>Instituto Federal de Educação, Ciência e Tecnologia da Paraíba</strong></p>
        <p style='text-align: center;'>Av. Primeiro de Maio, 720 - Jaguaribe, João Pessoa - PB, 58015-435</p>   
    </div>  
        
    
</div>  
</div>
</body>
</html>
