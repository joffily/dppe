<div class="row" style=" width: 100%; padding-left:15%;">
	<div>
		<br><br>
		
			<p>Registro > Pesquisar</p><br>
				<h2>Pesquisar Edital </h2>
				<br>
		<div class="row formulario">
			<div class="col-md-8">
			<?php 
				echo form_open("registro/pesquisacampo");

				echo "<table>";

				echo "<tr>";
				echo "<td colspan=2 width=30% style='padding-right: 15px'>";
				echo "<h3 for='titulo'>Titulo</h3>";
				echo "<hr>";
				echo "</td>";
				echo "</tr>";
				
				echo "<tr>";
				echo "<td colspan=2 width=30% style='padding-right: 15px'>";
				echo "<div class='form-group'>";
				echo form_input( array(
						"name" => "titulo",
						"class" => "form-control",
						"maxlength" => "70",
						"id" => "titulo",
						"placeholder" => "titulo contem"
				));
				echo "</div>";
				echo "</td>";
				echo "</tr>";



				echo "<tr>";
				echo "<td  width=30% style='padding-right: 15px'>";
				echo "<h3 for='dt_inicio'>Data Inicio</h3>";
				echo "<hr>";
				echo "</td>";

				echo "<td  width=30% style='padding-right: 15px'>";
				echo "<h3 for='dt_fim'>Data Finalização</h3>";
				echo "<hr>";
				echo "</td>";
				echo "</tr>";
				
				echo "<tr>";
				echo "<td width=30% style='padding-right: 15px'>";

				echo "<div class='form-group'>";
				echo form_input( array(
						"name" => "dt_inicio",
						"class" => "form-control",
						"maxlength" => "70",
						"id" => "example1",
						"placeholder" => "Inserir data inicio"
				));
				echo "</div>";
				echo "</td>";

				echo "<td width=30% style='padding-right: 15px'>";

				echo "<div class='form-group'>";
				echo form_input( array(
						"name" => "dt_fim",
						"class" => "form-control",
						"maxlength" => "70",
						"id" => "example2",
						"placeholder" => "Inserir data finalização"
				));
				echo "</div>";
				echo "</td>";
				echo "</tr>";
				echo "</table>";


				echo "<br>";
				echo form_button(array(
					"class" => "btn btn-primary",
					"content" => "Pesquisar",
					"type" => "submit"
				));
				
			
				echo form_close();
			?><br><br>
			</div>
		</div>

	</div>


<?php if(isset($registros) && gettype($registros) != 'boolean'): ?> 

		 <table id="example" class="table table-striped table-bordered" width=80% cellspacing="0">
				<thead>
				<tr>
				<th>TITULO</th>				
				<th>DATA INICIO</th>
				<th>DATA FIM</th>
				<th></th>
			</tr>
			</thead>  
			<tfoot>
				<tr>
				<th>TITULO</th>				
				<th>DATA INICIO</th>
				<th>DATA FIM</th>
				<th></th>
			</tr>
			 </tfoot> 
      <tbody>
		<?php foreach ($registros as $registro) : ?>
			<!-- registro -->
			<tr>				
				<td><?= $registro['titulo'] ?></td>
				<td><?= date('d/m/Y', strtotime($registro['dt_inicio'])); ?></td>
				<td><?= date('d/m/Y', strtotime($registro['dt_fim'])); ?></td>
				<td> <?=anchor("registro/mostra/{$registro['id']}", "Visualizar") ?><br>
				<?=anchor("registro/declaracaoregistro/{$registro['id']}", "Declaracao") ?>
				</td>
			</tr>
			<!-- .registro -->

		<?php endforeach;?>
		<?php endif ?>
		 </tbody>
		</table>
</div>

	<script type="text/javascript">
    $(document).ready(function() {

     $('#example1').datepicker({
         format: "dd/mm/yyyy"
      });  
     $('#example2').datepicker({
         format: "dd/mm/yyyy"
      }); 
     $('#example').dataTable();
            
} );
</script>
		
		
		
		
