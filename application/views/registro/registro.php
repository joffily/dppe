<div>
	<br><br>	
	<p>Registro > Gerenciar Registro > Visualizar Registro</p>
	<h2>Visualizar Registro</h2>
</div>
<br>
<div>
<div style=" background-color:#EEEEEE; position: relative; -moz-border-radius:4px; -webkit-border-radius:4px; 
 border-radius:4px; border: 2px solid #D1D1D1; ">
	 <div style=" font-weight: bold; padding-top:12px; padding-left:12px; border-bottom: 2px solid #D1D1D1; ">
		<b>Cadastro Registro</b>
	</div>
	<div style="  background-color:#fff; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Título:</b>
		<div class='form-group' style='padding-right: 15px'>
			<?php 
				echo $registro[0]['titulo'];
			?>
		</div>
	</div>	
		<div style=" background-color:#EEEEEE; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Data de Início:</b>	
		<div class='form-group' style='padding-right: 15px'>
		<?php
			echo  date('d/m/Y', strtotime($registro[0]['dt_inicio']));
		?>
		</div>
	</div>	
	<div style=" background-color:#EEEEEE; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Data de Finalização:</b>	
		<div class='form-group' style='padding-right: 15px'>
		<?php					
			echo  date('d/m/Y', strtotime($registro[0]['dt_fim']));
		?>
		</div>
	</div>	
	<div style="  background-color:#fff; padding-top:90px; padding-left:12px; padding-bottom:1px; ">
		<b>Objetivos Gerais:</b>
		<div class='form-group' style='padding-right: 15px'>
		<?php	
			echo $registro[0]['descricao'];
		?>
		</div>
		<?php
		echo form_error('descricao');
		?>
	</div>
	<div style=" background-color:#EEEEEE; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Edital:</b>	
		<div class='form-group' style='padding-right: 15px'>
			<a href="<?php echo base_url()."index.php/edital/visualizaredital/{$edital[0]['id']}" ?>"><?=$edital[0]['numero'] ?>/<?=$edital[0]['ano'] ?></a>				
		</div>
	</div>
	<div style="background: #fff;padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Arquivo digital:</b>	
		<div class='form-group' style='padding-right: 15px'>
			<a target="blank" href="<?php echo base_url() ?><?php echo $registro[0]['arquivo_url']; ?>">Visualizar arquivo</a>				
		</div>
	</div>
	<?php if (isset($participantes[0]->matricula)): ?>
	<div style="background-color:#EEEEEE; padding-top:12px; padding-left:12px; padding-bottom:1px;">
		<b>Participantes:</b>
			<table class="table" style="width:90%; margin: 3px;">
			<thead>
				<tr>
					<th>Matricula</th>
					<th>Nome</th>
					<th>Tipo</th>
					<th>Papel</th>
					<th>Data Início</th>
					<th>Data Fim</th>
				</tr>
			</thead>
			<tbody>			
				<?php foreach ($participantes as $item): ?>				
				<tr>
					<td>
						<a href="<?php echo base_url()."index.php/participante/visualizarparticipante/{$item->id}" ?>"><?php echo $item->matricula; ?></a>				
					</td>
					<td><?php echo $item->nome; ?></td>
					<td><?php echo $item->tipo; ?></td>
					<td><?php $dado = $this->registro_model->papeisNome($item->id_papel); echo $dado[0]->descricao; ?></td>
					<td><?php echo empty($item->dt_inicio) ? "-": date('d/m/Y', strtotime($item->dt_inicio)); ?></td>
					<td><?php echo empty($item->dt_fim) ? "-": date('d/m/Y', strtotime($item->dt_fim)); ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
			</table>					
	</div>
	<?php else: ?>
	<div style="background-color:#EEEEEE; padding-top:12px; padding-left:12px; padding-bottom:1px;">
		<p>Sem participantes cadastrados</p>
	</div>
	<?php endif; ?>

	</div>
	</div>
