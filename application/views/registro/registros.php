<div>
	<br><br>	
	<p>Registro > Gerenciar Registro</p>
	<h2>Registros</h2>
</div>

<br>

<p><a href="<?php echo base_url()."index.php/registro/cadastro/" ?>" id="dialog-link2" class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-circle-plus"></span>Adicionar Registro</a></p>
<br>

<div style=" min-height: 100%;background-color:#d9d9d9;  position: relative; -moz-border-radius:4px; -webkit-border-radius:4px; 
 border-radius:4px;">
 <!-- Resultado da pesquisa -->
    <table id="example" class="table display table-responsive dataTable" width="100%" cellspacing="0">
      	<thead style=" min-height: 100%;background-color:#EDEDEE;" >
			<tr >
				<th>Título</th>				
				<th>Data Início </th>
				<th>Data Fim </th>
				<th>Ações</th>
			</tr>
		</thead>  
	
      <tbody style="width: 100%;">
            
       <?php foreach ($registros as $registro) : ?>
			<!-- registro -->
			<tr>
				<?php if(strlen($registro['titulo']) > 35) : ?>				
				<td style="width: 45%;"><?= substr($registro['titulo'], 0, 35);  ?>...</td>
				<?php endif ?>
				<?php if(strlen($registro['titulo']) <= 35) : ?>				
				<td style="width: 45%;"><?= $registro['titulo'] ?></td>
				<?php endif ?>
				<td style="width: 15%; text-align: center;"><?= date('d/m/Y', strtotime($registro['dt_inicio'])); ?></td>
				<td style="width: 15%; text-align: center;"><?= date('d/m/Y', strtotime($registro['dt_fim'])); ?></td>
				<td style="width: 100%;" class='actions'> 
					<a href="<?php echo base_url()."index.php/registro/mostra/{$registro['id']}" ?>" id="dialog-link" class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-document"></span>Visualizar</a>
					<a href="<?php echo base_url()."index.php/registro/declaracaoregistro/{$registro['id']}" ?>" id="dialog-link" class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-clipboard"></span>Declaração</a>
					<a href="<?php echo base_url()."index.php/registro/editar/{$registro['id']}" ?>" id="dialog-link" class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-pencil"></span>Editar</a>
				</td>
			</tr>
			<!-- .registro -->
	   <?php endforeach;?>
      </tbody>
    </table>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').dataTable({
    	 "pagingType": "full_numbers"
    });
} );
</script>
<br>






