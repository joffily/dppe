<div>
	<br><br>	
	<p>Registro > Gerenciar Registro > Editar Registro</p>
	<h2>Editar Registro</h2>
</div>
<br>
<div>
<div class="error-list">
	<?php echo validation_errors(); ?>
</div>
<div style=" background-color:#EEEEEE; position: relative; -moz-border-radius:4px; -webkit-border-radius:4px; 
 border-radius:4px; border: 2px solid #D1D1D1; ">
	 <div style=" font-weight: bold; padding-top:12px; padding-left:12px; border-bottom: 2px solid #D1D1D1; ">
		Editar Registro
	</div>
	<?php 
		echo form_open_multipart("registro/atualizar/$registro");
	?>
	<div style="  background-color:#fff; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		Título*:
		<div class='form-group' style='padding-right: 15px'>
		<?php 
		echo form_input( array(
			"name" => "titulo",
			"class" => "form-control",
			"maxlength" => "70",
			"id" => "titulo",
			"value" => empty($_POST['titulo']) ? "" : $_POST['titulo'],
			"placeholder" => "T&iacute;tulo"
		));
		?>
		</div>
		<?php
		echo form_error('titulo');
		?>
	</div>
	<div style=" background-color:#EEEEEE; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		Data de Início*:	
		<div class='form-group' style='padding-right: 15px'>
		<?php
			echo form_input( array(
					"name" =>"dt_inicio",
					"class" => "form-control",
					"maxlength" => "20",
					"id" => "dt_inicio",
					"value" => empty($_POST['dt_inicio']) ? "" : $_POST['dt_inicio'],
					"placeholder" => "Adicionar Data Inicio"
			));
		?>
		</div>
		<?php
		echo form_error('dt_inicio');
		?>
	</div>	
	<div style=" background-color:#EEEEEE; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		Data de Finalização*:	
		<div class='form-group' style='padding-right: 15px'>
		<?php					
			echo form_input( array(
					"name" => "dt_fim",
					"class" => "form-control",
					"maxlength" => "70",
					"id" => "dt_fim",
					"value" => empty($_POST['dt_fim']) ? "" : $_POST['dt_fim'],
					"placeholder" => "Adicionar Data Finalização"
			));
		?>
		</div>
		<?php
		echo form_error('dt_fim');
		?>
	</div>
	<div style=" background-color:#FFF; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		Localização no Arquivo *:
		<div class='form-group' style='padding-right: 15px'>
			<?php
			echo form_input( array(
				"name" =>"localizacao_fisica",
				"class" => "form-control",
				"maxlength" => "30",
				"id" => "localizacao_fisica",
				"value" => $projeto["localizacao_fisica"],
				"placeholder" => "Localização no arquivo"
				));
				?>
			</div>
		</div>
		<div style=" background-color:#FFF; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
			Considerações *:
			<div class='form-group' style='padding-right: 15px'>
				<?php
				echo form_input(array(
					"name" => "consideracoes",
					"class" => "form-control",
					"maxlength" => "70",
					"id" => "consideracoes",
					"value" => $projeto["consideracoes"],
					"placeholder" => "Estado ou considerações sobre o arquivo"
					));
					?>
				</div>
			</div>	
	 <div style="  background-color:#EEE; padding-top:90px; padding-left:12px; padding-bottom:1px; ">
		Descrição*:
		<div class='form-group' style='padding-right: 15px'>
		<?php	
				echo form_textarea( array(
					"name" => "descricao",
					"class" => "form-control",
					"maxlength" => "600",
					"rows" => "4",
					"id" => "descricao",
					"value" => empty($_POST['descricao']) ? "" : $_POST['descricao'],
					"placeholder" => "Descri&ccedil;&atilde;o"
				));
		?>
		</div>
		<?php
		echo form_error('descricao');
		?>
	</div>
	<div class="projeto-upload">
		<label class="label-block">Upload do arquivo digital</label>
		<label for="id-projeto-arquivo" class="btn btn-default btn-lg file-upload-label">
			<span id="name">Enviar arquivo digital</span>
		</label>
		<input id="id-projeto-arquivo" name="projeto-arquivo" type="file" />
	</div>
	<div style="padding-top:12px; padding-left:12px; padding-bottom:1px;">
		<b>Edital:</b>		
		</br>
		<table class="table" style="width:90%;">
			<thead>
				<th>Id</th>
				<th>Ano</th>
				<th>Descricao</th>						
			</thead>
			<tbody>	
				<tr>
					<td><?php echo $edital['id']; ?></td>
					<td><?php echo $edital['ano']; ?></td>
					<td><?php echo $edital['options']['objetivos_gerais']; ?></td>						
				</tr>	
			</tbody>
		</table>
	</div>
	<div style="background-color:#fff; padding-top:12px; padding-left:12px; padding-bottom:1px;">
		<b>Participantes Cadastrados:</b>
		</br>
		<table class="table" style="width:90%;">
			<thead>
				<tr>
					<th>Matricula</th>
					<th>Nome</th>
					<th>Tipo</th>
					<th>Papel</th>
					<th>Data Início</th>
					<th>Data Fim</th>
				</tr>
			</thead>
			<tbody>			
				<?php foreach ($participantes as $item): ?>				
				<tr>
					<td><?php echo $item->matricula; ?></td>
					<td><?php echo $item->nome; ?></td>
					<td><?php echo $item->tipo; ?></td>
					<td><?php $dado = $this->registro_model->papeisNome($item->id_papel); echo $dado[0]->descricao; ?></td>
					<td><?php echo empty($item->dt_inicio) ? "-": date('d/m/Y', strtotime($item->dt_inicio)); ?></td>
					<td><?php echo empty($item->dt_fim) ? anchor("registro/fecharParticipacao/$registro/$item->id", "Encerrar") : date('d/m/Y', strtotime($item->dt_fim)); ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<div style=" padding-top:12px; padding-left:12px; padding-bottom:1px;">
		<b>Inserir Professor/Servidor(Participante)*:</b>
		</br>
		<div style="width:90%;border-bottom:1px solid #D1D1D1; padding:20px; padding-left:12px; ">
		<a href="#inserirProfessor"><input type='button' value='+ Adicionar Professor/Servidor' class='btn btn-success'></a>
		</div>	
		<br>
		<div id="inserirProfessor" class="modalDialog">
		   <div>				
			<h3>Inserir Professor/Servidor(Participante)</h3>
			<table class='table' id='pesquisaProfessor'>
				<tr>
					<td style='padding-right: 15px' colspan=2>
						<div class='input_container'>
					    	<input type='text' placeholder='Digite a matricula ou o nome do Professor' id='professor_id' onkeyup='autocProfessor()' autocomplete='off' class='form-control'>
					        <ul id='professor_list_id'></ul>
				        </div>
					</td>
				</tr>
				<tr>
					<td style='padding-right: 15px' >
						<?php $papeis = $this->registro_model->papeisProjetoProfessor(); ?>
						<select name=paprof[] id=paprof class='form-control' autocomplete='off'>
			  				<option value='erro' class='form-control' selected='selected'>Definir o Papel</option>
			  				<?php foreach($papeis as $linha): ?>
			  				<option value=<?php echo $linha->id; ?> ><?php echo $linha->descricao; ?></option>
			  			    <?php endforeach; ?>
						</select>
						
					</td>
					<td style='padding-right: 15px'>
						<?php echo form_input( array(
								"name" => "dt_prof",
								"class" => "form-control",
								"maxlength" => "70",
								"id" => "dt_prof",
								"placeholder" => "Data Inicio"//,
								//"autocomplete" => "off"
							));
						?>
					</td>
				</tr>
				<tr>
					<td style='width: 10%; padding-right: 25px'><a title='Fechar' href='#'><input type='button' value='Inserir' class='btn btn-success' onClick='javascript:novaLinhaProfessor(); ResetModal()'></a></td>
					<td style='width: 10%; padding-right: 25px'><a title='Fechar' href='#'><input type='button' value='Cancelar' class='btn btn-danger' onClick='ResetModal()'></a></td>
				</tr>
			</table>
		   </div>
		</div>
		<?php foreach ($this->cart->contents() as $items): ?>
			<?php if($items['name'] == "professor"): ?>
				<table id="tbProfessorResult" class="table"  style="width:90%;">
					<thead>
						<th>Siape</th>
						<th>Nome</th>
						<th>Data</th>
						<th>Papel</th>
						<th>Acao</th>
					</thead>
					<tbody>	
						<tr>
							<td><?php echo $items['id']; ?></td>
							<?php if(strlen( $items['options']['nome']) > 40) : ?>				
								<td><?= substr( $items['options']['nome'], 0, 40);  ?>...</td>
							<?php endif ?>
							<?php if(strlen( $items['options']['nome']) <= 40) : ?>				
								<td><?=  $items['options']['nome']?></td>
							<?php endif ?>
							<td><?php echo $items['options']['data']; ?></td>
							<td><?php echo $items['options']['papel']; ?></td>
							<td><a href="<?php echo site_url()."/ajax/removeProfessor/".$items['rowid']; ?>"><input type='button' value='Remover' class='btn btn-danger' style="font-size:11px; "></a></td>
						</tr>	
					</tbody>
				</table>
			<?php endif; ?>
		<?php endforeach; ?>
	</div>
	<div style="background-color:#fff; padding-top:12px; padding-left:12px; padding-bottom:1px;">
		<b>Inserir Aluno(Participante):</b>
		</br>
		<div style="width:90%;border-bottom:1px solid #D1D1D1; padding:20px; padding-left:12px; ">
		<a href="#inserirParticipante"><input type='button' value='+ Adicionar Participante' class='btn btn-success'></a>
		</div>	
		</br>
		<div id="inserirParticipante" class="modalDialog">
		  <div>
			<h3>Inserir Aluno (Participante)</h3>
			<table class='table' id='Participante'>
				<tr>
					<td style='padding-right: 15px' colspan=2>
						<div class='input_container'>
					    	<input type='text' placeholder='Digite a matricula ou o nome do Participante' id='participante_id' onkeyup='autocParticipante()' autocomplete='off' class='form-control'>
					        <ul id='participante_list_id'></ul>
				        </div>
					</td>
				</tr>
				<tr>
					<td style='padding-right: 15px' >
						<?php $papeis = $this->registro_model->papeisProjetoParticipante(); ?>
						<select name=paparticiapante[] id=paparticipante class='form-control' autocomplete='off'>
			  				<option value='erro' class='form-control' selected='selected'>Definir o Papel</option>
			  				<?php foreach($papeis as $linha): ?>
			  				<option value=<?php echo $linha->id; ?> ><?php echo $linha->descricao; ?></option>
			  			    <?php endforeach; ?>
						</select>
						
					</td>
					<td style='padding-right: 15px'>
						<?php echo form_input( array(
								"name" => "dt_participante",
								"class" => "form-control",
								"maxlength" => "70",
								"id" => "dt_participante",
								"placeholder" => "Data Inicio"//,
								//"autocomplete" => "off"
							));
						?>
					</td>
				</tr>
				<tr>
					<td style='width: 10%; padding-right: 25px'><a title='Fechar' href='#'><input type='button' value='Inserir' class='btn btn-success' onClick='javascript:novaLinhaParticipante(); ResetModal()'></a></td>
					<td style='width: 10%; padding-right: 25px'><a title='Fechar' href='#'><input type='button' value='Cancelar' class='btn btn-danger' onClick='ResetModal()'></a></td>
				</tr>
			</table>
		   </div>
		</div>
		<?php foreach ($this->cart->contents() as $items): ?>
			<?php if($items['name'] == "participante"): ?>
				<table id="tbParticipanteResult" class="table"  style="width:90%;">
					<thead>
						<th>Matricula</th>
						<th>Nome</th>
						<th>Data</th>
						<th>Papel</th>
						<th>Acao</th>
					</thead>
					<tbody>	
						<tr>
							<td><?php echo $items['id']; ?></td>
							<?php if(strlen( $items['options']['nome']) > 40) : ?>				
								<td><?= substr( $items['options']['nome'], 0, 40);  ?>...</td>
							<?php endif ?>
							<?php if(strlen( $items['options']['nome']) <= 40) : ?>				
								<td><?=  $items['options']['nome']?></td>
							<?php endif ?>							
							<td><?php echo $items['options']['data']; ?></td>
							<td><?php echo $items['options']['papel']; ?></td>
							<td><a href="<?php echo site_url()."/ajax/removeParticipante/".$items['rowid']; ?>"><input type='button' value='Remover' class='btn btn-danger' style="font-size:11px; "></a></td>
						</tr>	
					</tbody>
				</table>
			<?php endif; ?>
		<?php endforeach; ?>
	</div>
	<div style=" background-color:#fff; padding-top:12px; padding-bottom:1px;">
	  <div style="background-color:#fff; padding-top:12px; padding-bottom:1px; border-top: 1px solid  #D1D1D1;">		
		<br>
		<div class='form-group' style='text-align:center'>
		<?php		
			echo form_button(array(
				"class" => "btn btn-primary",
				"content" => "Atualizar",
				"type" => "submit"
			));
		?>
		</div>
		<?php			
			echo form_close();
		?>
		<br>
	  </div>
	</div>
</div>
</div>


<script type="text/javascript">	
	CKEDITOR.replace("descricao");

	var titulo;
	var dt_inicio;
	var dt_fim;
	var descricao;

	function autocEdital() {
		var min_length = 0; // min caracters to display the autocomplete
		var keyword = $('#edital_id').val();
		if (keyword.length >= min_length) {
			$.ajax({
				url: '<?php echo site_url()."/ajax/getEdital"?>',
				type: 'POST',
				data: {keyword:keyword},
				success:function(data){
					$('#edital_list_id').show();
					$('#edital_list_id').html(data);
				}
			});
		} else {
			$('#edital_list_id').hide();
		}
	}

	function novaLinhaEdital() {

		dado = $('#edital_id').val();
		titulo = $('#titulo').val();
		dt_inicio = $('#dt_inicio').val();
		dt_fim = $('#dt_fim').val();
		descricao = $('#descricao').val();
		
		if (dado != "") {
			$.ajax({
				url: '<?php echo site_url()."/ajax/setEditalSessao"?>',
				type: 'POST',
				data: {dadoEdital:dado},
				success:function(data){
					location.reload(); 
					$('#titulo').val() = titulo;
					$('#dt_inicio').val() = dt_inicio;
					$('#dt_fim').val() = dt_fim;
					$('#descricao').val() = descricao;
				}
			});
		} else {
			alert("Selecione um edital válido");
			$('#edital_list_id').hide();
		}
	}

	// set_item : this function will be executed when we select an item
	function set_item_edital(item) {

		// change input value
		$('#edital_id').val(item);
		// hide proposition list
		$('#edital_list_id').hide();
	}

	function autocProfessor() {
		var min_length = 0; // min caracters to display the autocomplete
		var keyword = $('#professor_id').val();
		if (keyword.length >= min_length) {
			$.ajax({
				url: '<?php echo site_url()."/ajax/getProfessor"?>',
				type: 'POST',
				data: {keyword:keyword},
				success:function(data){
					$('#professor_list_id').show();
					$('#professor_list_id').html(data);
				}
			});
		} else {	
			$('#professor_list_id').hide();
		}
	}

	function novaLinhaProfessor() {

		var professor = $('#professor_id').val();
		var papel = $('#paprof').val();
		var data = $('#dt_prof').val();

		var sendInfo = {
			nome: professor,
			papel: papel,
			data: data
		};
		
		if (papel != "erro" && professor != "" &&  data != "") { 		
			$.ajax({
				url: '<?php echo site_url()."/ajax/setProfessorSessao"?>',
				type: 'POST',
				data: {dadosProfessor:sendInfo},
				success: function() {
					titulo = $('#titulo').val();
					dt_inicio = $('#dt_inicio').val();
					dt_fim = $('#dt_fim').val();
					descricao = $('#descricao').val();
	    			//window.location.reload(true);
	    			location.reload();
					$('#titulo').val() = titulo;
					$('#dt_inicio').val() = dt_inicio;
					$('#dt_fim').val() = dt_fim;
					$('#descricao').val() = descricao;
				}
			});		
		} else {
			alert("Preencher os dados do Professor Corretamente");
			$('#edital_list_id').hide();
		}
	}


	function novaLinhaParticipante() {
		titulo = $('#titulo').val();
		dt_inicio = $('#dt_inicio').val();
		dt_fim = $('#dt_fim').val();
		descricao = $('#descricao').val();

		var participante = $('#participante_id').val();
		var papel = $('#paparticipante').val();
		var data = $('#dt_participante').val();

		var sendInfo = {
			nome: participante,
			papel: papel,
			data: data
		};
		
		if (papel != "erro" && participante != "" &&  data != "") { 		
			$.ajax({
				url: '<?php echo site_url()."/ajax/setParticipanteSessao"?>',
				// dataType: "json",
				type: 'POST',
				data: {dadosParticipante:sendInfo},
				success: function() {
	    			// window.location.reload(true);
	    			location.reload();
					$('#titulo').val() = titulo;
					$('#dt_inicio').val() = dt_inicio;
					$('#dt_fim').val() = dt_fim;
					$('#descricao').val() = descricao;
				}
			});		
		} else {
			alert("Preencher os dados do Participante Corretamente");
			$('#participante_list_id').hide();
		}
	}

		// set_item : this function will be executed when we select an item
	function set_item_participante(item) {

		// change input value
		$('#participante_id').val(item);
		// hide proposition list
		$('#participante_list_id').hide();
	}

	function autocParticipante() {
		var min_length = 0; // min caracters to display the autocomplete
		var keyword = $('#participante_id').val();
		if (keyword.length >= min_length) {
			$.ajax({
				url: '<?php echo site_url()."/ajax/getAlunos"?>',
				type: 'POST',
				data: {keyword:keyword},
				success:function(data){
					$('#participante_list_id').show();
					$('#participante_list_id').html(data);
				}
			});
		} else {
			$('#participante_list_id').hide();
		}
	}	



	// set_item : this function will be executed when we select an item
	function set_item_professor(item) {

		// change input value
		$('#professor_id').val(item);
		// hide proposition list
		$('#professor_list_id').hide();
	}


	function action(id){
	    var el = document.getElementById(id);
	    (el.style.display == 'none') ? el.style.display = 'block' : el.style.display = 'none';
	}

	function ResetModal() {

        $("#example3").each(function () {
            $(this).val("");
        });
 
        $("#pap").each(function () {
           $(this).val("erro");
        });
    }


	$(document).ready(function(){
		$('#dt_inicio').datepicker({
         format: "dd/mm/yyyy"
    	  });  
  		$('#dt_fim').datepicker({
   	      format: "dd/mm/yyyy"
   		   }); 
  		$('#dt_participante').datepicker({
   	      format: "dd/mm/yyyy"
   		   }); 
  		$('#dt_prof').datepicker({
   	      format: "dd/mm/yyyy"
   		   });
	});

$("#complexConfirm").confirm({
    title:"Concluir participação",
    text:"Deseja realmente finalizar a participação?",
    confirm: function(button) {
        alert("finalizada");
    },
    cancel: function(button) {
        
    },
    confirmButton: "Sim, Eu quero",
    cancelButton: "Não"
});

</script>


