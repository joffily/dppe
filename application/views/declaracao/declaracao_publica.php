<style media="screen">
  .center {
    width: 800px;
    margin: 0 auto;
  }

  .table {
    width: 100%;
  }
</style>
<div class="row center">
  <div align="justify">
    <!-- registro -->
    <div align="center">
      <img src="<?= base_url("assets/img/brasil.png")?>" style="width: 100px;">
      <br><font size="5"><?php echo $dppe[6]->dados; ?></font>
      <br><font size="4"><?php echo $dppe[0]->dados; ?></font>
      <br><font size="3"><?php echo $dppe[2]->dados; ?></font>
    </div>
    <br><br><br><br><br>

<?php echo $texto; ?>

<table class="table">
  <tr>
    <th>Papel</th>
    <th colspan="3">Nome Registro</th>
    <th>Data de Início</th>
    <th>Data Fim</th>
    <th>Edital</th>
  </tr>

  <?php
    foreach ($projetos as $key => $projeto) {
      if ($projeto['papel'] == '1') {
        $papel = "Orientador";
      } elseif ($projeto['papel'] == '2') {
        $papel = "Bolsista";
      } elseif ($projeto['papel'] == '3') {
        $papel = "Voluntário";
      } else {
        $papel = "Servidor";
      }
  ?>
  <tr>
    <td>
      <?php echo $papel;?>
    </td>
    <td colspan='3'>
      <?php echo $projeto['titulo'];?>
    </td>
    <td>
      <?php echo date('d/m/Y', strtotime($projeto['dt_inicio']));?>
    </td>
    <td>
      <?php
        if($projeto['dt_fim'] != "--"){
          echo date('d/m/Y', strtotime($projeto['dt_fim']));
        }
        if($projeto['dt_fim'] == "--"){
          echo $projeto['dt_fim'];
        }
      ?>
      </td>
      <td>
        <?php echo $projeto['numeroedital']; ?>/<?php echo $projeto['anoedital']; ?>
      </td>
  </tr>
  <?php } ?>
</table>

<br><br><br><br>
<div>
  <p>Encontra-se devidamente registrado nesse departamento.</p>
</div>
<br><br>

<div style="text-align:right; padding-right:20% ">
  <?php echo $data; ?>
</div>
<br><br><br><br>
<div id="assinatura" align="center">
  <p>Chave verificadora</p>
  <p><?php echo $hash; ?><p>
</div>

<br><br><br><br>
