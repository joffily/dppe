
<div>
	<br><br>	
	<p>Registro > Gerenciar Participante > Declaração Participante</p>
	<h1>Declaração Participante</h1>
</div>
<br>
<br>
<p><a href="<?php echo base_url()."index.php/participante/declaracaoParticipantePdf/".$participante[0]['id']; ?>" id="dialog-link2" class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-print"></span>Imprimir em PDF</a></p>

<div class="row">
	<div align="justify">
		<!-- registro -->
			<div align="center">	
				<img src="<?= base_url("assets/img/brasil.png")?>" style="width: 100px;">			
				<br><font size="5"><?php echo $dppe[6]->dados; ?></font>
				<br><font size="4"><?php echo $dppe[0]->dados; ?></font>
				<br><font size="3"><?php echo $dppe[2]->dados; ?></font>
			</div>
			
			<br>
			<br>
			<br>
			<br>		
			<br>
			<?php if(count($registro) != 0) : ?>
			<div style="width: 800px; margin-left: 100px;">
				Eu, <?php echo $dppe[1]->dados; ?>, coordenador do <?php echo $dppe[2]->dados; ?>, do <?php echo $dppe[5]->dados; ?>, declaro
				que <b> <?=$participante[0]['nome'] ?></b>, matricula: <?=$participante[0]['matricula'] ?>.
				<p>Possuí o(s) seguinte(s) registro(s) nesse Departamento:</p></br>
			</div>
			
			<table class="table" style="width: 800px; margin-left: 100px;">
				<tr>
					<th>Papel</th>
					<th colspan="3">Nome Registro</th>
					<th>Data de Início</th>
					<th>Data Fim</th>
					<th>Edital</th>
				</tr>
				
				<?php
					for ($i=0; $i < count($registro) ; $i++) { 
						switch ($registro[$i]['papel']) {
							case '1':
								$papel = "Orientador";
								break;
							case '2':
								$papel = "Bolsista";
								break;
							case '3':
								$papel = "Voluntário";
								break;
							case '4':
								$papel = "Servidor";
								break;						
						}
						?>
						<tr><td>
						<?php echo $papel;?>
						</td><td colspan='3'>
						<?php echo $registro[$i]['titulo'];?>
						</td><td>
						<?php echo date('d/m/Y', strtotime($registro[$i]['dt_inicio']));?>
						</td><td>
						<?php if($registro[$i]['dt_fim'] != "--"){
						echo date('d/m/Y', strtotime($registro[$i]['dt_fim']));
						}
						if($registro[$i]['dt_fim'] == "--"){
						echo $registro[$i]['dt_fim'];
						}
						?>
						</td>						
						<td>
						<?=$registro[0]['numeroedital'] ?>/<?=$registro[0]['anoedital'] ?>
						</td>
						</tr>
						<?php
					}
				?>			

			</table>	
						<br>
			<br>
			<br>
			<br>	
			<div style="width: 800px; margin-left: 100px;">
				<p>Encontra-se devidamente registrado nesse departamento.</p>
			</div>
			<br> 
			<br> 

			<?php else: ?>
			<div style="width: 800px; margin-left: 100px;">
				Eu, <?php echo $dppe[1]->dados; ?>, coordenadora do <?php echo $dppe[2]->dados; ?>, do <?php echo $dppe[5]->dados; ?>, declaro
				que <b> <?=$participante[0]['nome'] ?></b>, matricula: <?=$participante[0]['matricula'] ?>.
				<p>Não possuí nenhum registro neste departamento</p></br>
			</div>	
			<?php endif ?>

			<div style="text-align:right; padding-right:20% "> 
				<?php $date = date('y-m-d'); 
				echo "João Pessoa".utf8_encode(strftime(", %d de %B de %Y", strtotime($date))); ?> 
			</div> 
			<br> 
			<br> 
			<br> 
			<br>
			<div id="assinatura" align="center">
				<p>______________________________________________________<p>
			    <b><font size="3"><?php echo $dppe[1]->dados; ?></font></b>
			    <br>Coordenadora <?php echo $dppe[2]->dados; ?>

			</div>

			<br>
			<br>
			<br>
			<br>

			
		<!-- .registro -->
	</div>
</div>