<!doctype html>
<html lang="pt-BR">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >

</head>
<body>

<div class="row">
	<div align="justify">
		<!-- registro -->
			<div align="center">	
				<img src="./assets/img/brasil.png" style="width: 100px;">			
				<br><font size="5"><?php echo $dppe[6]->dados; ?></font>
				<br><font size="4"><?php echo $dppe[0]->dados; ?></font>
				<br><font size="3"><?php echo $dppe[2]->dados; ?></font>
			</div>
			
			<br>
			<br>
			<br>
			<br>		
			<br>
			<div>
				Eu, <?php echo $dppe[1]->dados; ?>, coordenadora do <?php echo $dppe[2]->dados; ?>, do <?php echo $dppe[5]->dados; ?>, declaro
				que o projeto: <?=$registro[0]['titulo'] ?>, com data de início: <?php echo date('d/m/Y', strtotime($registro[0]['dt_inicio'])) ?>
				e data de termíno: <?php echo date('d/m/Y', strtotime($registro[0]['dt_fim']))?>, estabelecido pelo edital de número: 
				<?=$edital[0]['numero'] ?>/<?=$edital[0]['ano'] ?>. 
				<p>Possuí os seguintes integrantes:
			</div>
			
			<table class="table" align="center" width="100%" border="1">
				<tr>
					<td>Papel</td>
					<td>Matrícula</td>
					<td>Nome</td>					
				</tr>
				
				<?php
					for ($i=0; $i < count($participantes) ; $i++) { 
						switch ($participantes[$i]['papel']) {
							case '1':
								$papel = "Orientador";
								break;
							case '2':
								$papel = "Bolsista";
								break;
							case '3':
								$papel = "Voluntário";
								break;
							case '4':
								$papel = "Servidor";
								break;						
						}


						echo "<tr><td>";
						echo $papel;
						echo "</td><td>";
						echo $participantes[$i]['matricula'];
						echo "</td><td>";
						echo strtoupper($participantes[$i]['nome']);
						echo "</td>";
						echo "</tr>";
					}
				?>
			</table>	
			<br>
			<br>
			<br>
			<br>	
			<div>
				<p>Encontra-se devidamente registrado nesse departamento.
			</div>
			<br> 
			<br> 
			<br> 
			<br> 
			<div style="text-align:right; padding-right:20% "> 
				<?php $date = date('y-m-d'); 
				echo "João Pessoa".utf8_encode(strftime(", %d de %B de %Y", strtotime($date))); ?> 
			</div>
			<br> 
			<br> 
			<br> 
			<br>
			<div id="assinatura" align="center">
				<p>______________________________________________________</p>
			    <b><font size="3"><?php echo $dppe[1]->dados; ?></font></b>
			    <br>Coordenadora <?php echo $dppe[2]->dados; ?>
			</div>

			<br>
			<br>
			<br>
			<br>
			

</body>
</html>