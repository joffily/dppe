<div class="page-header">
  <h1>Validar declaração</h1>
</div>

<?php echo form_open("declaracao/validar"); ?>
  <div class="form-group">
    <label for="exampleInputEmail1">Chave verificadora</label>
    <input autocomplete="off" class="form-control" type="text" name="hash" placeholder="Digite a sua chave verificadora" required>
  </div>

  <!-- div class="form-group">
    <div class="g-recaptcha" data-sitekey="6LcpIyITAAAAANrCsDLoNzPa5sWh3VmHT3ytrO99"></div>
  </div-->
  <button type="submit" class="btn btn-primary">Validar</button>
<?php echo form_close(); ?>
