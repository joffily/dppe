<div class="page-header">
  <h1>Emitir declaração</h1>
</div>

<?php echo form_open("declaracao/emitir"); ?>
  <div class="form-group">
    <label for="exampleInputEmail1">Matrícula</label>
    <input autocomplete="off" class="form-control" type="text" name="matricula" placeholder="Digite sua matrícula" required>
  </div>

  <!-- div class="form-group">
    <div class="g-recaptcha" data-sitekey="6LcpIyITAAAAANrCsDLoNzPa5sWh3VmHT3ytrO99"></div>
  </div-->
  <button type="submit" class="btn btn-primary">Emitir</button>
<?php echo form_close(); ?>
