<div class="page-header">
  <h1>Escolha uma das opções</h1>
</div>

<div class="container">
  <div class="col-md-2">
  </div>
  <div class="col-md-3">
    <a class="btn btn-lg btn-success" href="<?php echo site_url("/declaracao/emitir"); ?>">Emitir declaração</a>
  </div>
  <div class="col-md-3">
    <a class="btn btn-lg btn-success" href="<?php echo site_url("/declaracao/validar"); ?>">Validar declaração</a>
  </div>
</div>
