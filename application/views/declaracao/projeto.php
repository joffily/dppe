<div>
	<br><br>	
	<p>Registro > Gerenciar Registro > Declaração Registro</p>
	<h2>Declaração Registro</h2>
</div>
<br>
<br>
<p><a href="<?php echo site_url(). '/registro/declaracaoRegistroPdf/'. $registro[0]['id']; ?>" id="dialog-link2" class="ui-state-default ui-corner-all"><span class="ui-icon ui-icon-print"></span>Imprimir em PDF</a></p>


<div class="row">
	<div align="justify">
		<!-- registro -->
			<div align="center">	
				<img src="<?php echo base_url(). "assets/img/brasil.png"?>" style="width: 100px;">			
				<br><font size="5"><?php echo $dppe[6]->dados; ?></font>
				<br><font size="4"><?php echo $dppe[0]->dados; ?></font>
				<br><font size="3"><?php echo $dppe[2]->dados; ?></font>
			</div>
			
			<br>
			<br>
			<br>
			<br>		
			<br>
			<div style="width: 800px; margin-left: 100px;">
				Eu, <?php echo $dppe[1]->dados; ?>, coordenadora do <?php echo $dppe[2]->dados; ?>, do <?php echo $dppe[5]->dados; ?>, declaro
				que o projeto:<b> <?=$registro[0]['titulo'] ?></b>, com data de início: <?php echo date('d/m/Y', strtotime($registro[0]['dt_inicio'])) ?>
				e data de termíno: <?php echo date('d/m/Y', strtotime($registro[0]['dt_fim']))?>, estabelecido pelo edital de número: 
				<?=$edital[0]['numero'] ?>/<?=$edital[0]['ano'] ?>. 
				<p>Possuí os seguintes integrantes:</p></br>
			</div>
			
			<table class="table" style="width: 800px; margin-left: 100px;">
				<tr>
					<th>Papel</th>
					<th>Matrícula</th>
					<th>Nome</th>					
				</tr>
				
				<?php
					for ($i=0; $i < count($participantes) ; $i++) { 
						switch ($participantes[$i]['papel']) {
							case '1':
								$papel = "Orientador";
								break;
							case '2':
								$papel = "Bolsista";
								break;
							case '3':
								$papel = "Voluntário";
								break;
							case '4':
								$papel = "Servidor";
								break;						
						}


						echo "<tr><td>";
						echo $papel;
						echo "</td><td>";
						echo $participantes[$i]['matricula'];
						echo "</td><td>";
						echo $participantes[$i]['nome'];
						echo "</td>";
						echo "</tr>";
					}
				?>
			</table>	
			<br>
			<br>
			<br>
			<br>	
			<div style="width: 800px; margin-left: 100px;">
				<p>Encontra-se devidamente registrado nesse departamento.</p>
			</div>
			<br> 
			<br> 
			<div style="text-align:right; padding-right:20% "> 
				<?php 
				$date = date('y-m-d'); 
				echo "João Pessoa".utf8_encode(strftime(", %d de %B de %Y", strtotime($date))); ?> 
			</div> 
			<br> 
			<br> 
			<br> 
			<br>
			<div id="assinatura" align="center">
				<p>______________________________________________________<p>
			    <b><font size="3"><?php echo $dppe[1]->dados; ?></font></b>
			    <br>Coordenadora <?php echo $dppe[2]->dados; ?>
			</div>

			<br>
			<br>
			<br>
			<br>
			

		<!-- .registro -->
	</div>
</div>