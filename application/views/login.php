<style type="text/css">
input {
    height: 40px !important;
}
</style>
<nav class="navbar navbar-default" role="navigation">
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="<?php echo site_url('declaracao')?>">Declaração</a>
            </li>

        </ul>
    </div>

</nav>
<br><br><br><br>
        <div style=" width:350px; background-color:#EEEEEE; position: relative; -moz-border-radius:4px; -webkit-border-radius:4px;
            border-radius:4px; border: 2px solid #D1D1D1; margin:auto; ">

            <?php
                echo form_open("login/logar", array(
                    "class" => "form-signin",
                    "role" => "form"
                ));
            ?>
                <div class='form-group'>
                <?=form_input( array(
                    "name" => "login",
                    "class" => "form-control",
                    "maxlength" => "70",
                    "id" => "login",
                    "placeholder" => "Login"
                ));?>
                </div>

                <div class='form-group'>
                <?=form_password( array(
                    "name" => "senha",
                    "class" => "form-control",
                    "maxlength" => "70",
                    "id" => "senha",
                    "placeholder" => "Senha"
                ));?>
                </div>
                <br>
                <div style="text-align:center;">
                <?=form_button(array(
                    "class" => "btn btn-primary",
                    "content" => "Entrar",
                    "type" => "submit"
                ));?>
                </div>
                <?=form_close();?>
            <br>
            </div>
