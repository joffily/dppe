<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
</head>

<body>

<div id="container">
<!-- Edital -->

<!-- <form action="setEditalSessao" method="POST">
	<input type="text" id="edital" name="edital">
	<input type="submit" value="Pesquisar edital">
</form>
 -->

				
<table class='table' id='pesquisaEdital'>
	<h3>Inserir Edital</h3>
	<tr>
	<td style='padding-right: 15px' colspan=2>
	<div class='input_container'>
	<input type='text' id='edital_id' onkeyup='autocEdital()' autocomplete='off' class='form-control' placeholder='Digite o número ou objetivo'>
	<ul id='edital_list_id'></ul>
	</div>
	</td>
	</tr>
	<tr>
	<td style='width: 10%; padding-right: 25px'>
	<a title='Fechar' href='#'><input type='button' value='Inserir' class='btn btn-success' onClick='javascript:novaLinhaEdital()'></a></td>
	<td style='width: 10%; padding-right: 25px'><a title='Fechar' href='#'><input type='button' value='Cancelar' class='btn btn-danger'></a></td></tr>
</table>

<table id="tbEditalResult" border="1">
	<thead>
			<td>Id</td>
			<td>Descricao</td>
			<td>Acao</td>
	</thead>
<tbody>

	<?php foreach ($this->cart->contents() as $items): ?>

		<?php if($items['name'] == "edital"): ?>
			<tr>
				<td><?php echo $items['id']; ?></td>
				<td><?php echo $items['options']['objetivos_gerais']; ?></td>
				<td><a href="removeEdital/<?php echo $items['rowid']; ?>">remover</a></td>
			</tr>	
		<?php endif; ?>

	<?php endforeach; ?>
</tbody>
</table>



<!-- Professor -->

<h3>Inserir Professor</h3>
	<table class='table' id='pesquisaProfessor'>
		<tr>
			<td style='padding-right: 15px' colspan=2>
				<div class='input_container'>
			    	<input type='text' placeholder='Digite a matricula ou o nome do Professor' id='professor_id' onkeyup='autocProfessor()' autocomplete='off' class='form-control'>
			        <ul id='professor_list_id'></ul>
		        </div>
			</td>
		</tr>
		<tr>
			<td style='padding-right: 15px' >
				<?php $papeis = $this->registro_model->papeisProjetoProfessor(); ?>
				<select name=paprof[] id=paprof class='form-control' autocomplete='off'>
	  				<option value='erro' class='form-control' selected='selected'>Definir o Papel</option>
	  				<?php foreach($papeis as $linha): ?>
	  				<option value=<?php echo $linha->id; ?> ><?php echo $linha->descricao; ?></option>
	  			    <?php endforeach; ?>
				</select>
				
			</td>
			<td style='padding-right: 15px'>
				<?php echo form_input( array(
						"name" => "dt_prof",
						"class" => "form-control",
						"maxlength" => "70",
						"id" => "dt_prof",
						"placeholder" => "Data Inicio"//,
						//"autocomplete" => "off"
					));
				?>
			</td>
		</tr>
		<tr>
			<td style='width: 10%; padding-right: 25px'><a title='Fechar' href='#'><input type='button' value='Inserir' class='btn btn-success' onClick='javascript:novaLinhaProfessor(); ResetModal()'></a></td>
			<td style='width: 10%; padding-right: 25px'><a title='Fechar' href='#'><input type='button' value='Cancelar' class='btn btn-danger' onClick='ResetModal()'></a></td>
		</tr>
	</table>


	<table id="tbProfessorResult" border="1">
		<thead>
			<td>Siape</td>
			<td>Nome</td>
			<td>Data</td>
			<td>Papel</td>
			<td>Acao</td>
		</thead>
		<tbody>	
			<?php foreach ($this->cart->contents() as $items): ?>
				<?php if($items['name'] == "professor"): ?>
					<tr>
						<td><?php echo $items['id']; ?></td>
						<td><?php echo $items['options']['nome']; ?></td>
						<td><?php echo $items['options']['data']; ?></td>
						<td><?php echo $items['options']['papel']; ?></td>
						<td><a href="removeProfessor/<?php echo $items['rowid']; ?>">remover</a></td>
					</tr>	
				<?php endif; ?>
			<?php endforeach; ?>
		</tbody>
	</table>


<!-- Participante -->

<h3>Inserir Participante</h3>
	<table class='table' id='Participante'>
		<tr>
			<td style='padding-right: 15px' colspan=2>
				<div class='input_container'>
			    	<input type='text' placeholder='Digite a matricula ou o nome do Participante' id='participante_id' onkeyup='autocParticipante()' autocomplete='off' class='form-control'>
			        <ul id='participante_list_id'></ul>
		        </div>
			</td>
		</tr>
		<tr>
			<td style='padding-right: 15px' >
				<?php $papeis = $this->registro_model->papeisProjetoParticipante(); ?>
				<select name=paparticiapante[] id=paparticipante class='form-control' autocomplete='off'>
	  				<option value='erro' class='form-control' selected='selected'>Definir o Papel</option>
	  				<?php foreach($papeis as $linha): ?>
	  				<option value=<?php echo $linha->id; ?> ><?php echo $linha->descricao; ?></option>
	  			    <?php endforeach; ?>
				</select>
				
			</td>
			<td style='padding-right: 15px'>
				<?php echo form_input( array(
						"name" => "dt_participante",
						"class" => "form-control",
						"maxlength" => "70",
						"id" => "dt_participante",
						"placeholder" => "Data Inicio"//,
						//"autocomplete" => "off"
					));
				?>
			</td>
		</tr>
		<tr>
			<td style='width: 10%; padding-right: 25px'><a title='Fechar' href='#'><input type='button' value='Inserir' class='btn btn-success' onClick='javascript:novaLinhaParticipante(); ResetModal()'></a></td>
			<td style='width: 10%; padding-right: 25px'><a title='Fechar' href='#'><input type='button' value='Cancelar' class='btn btn-danger' onClick='ResetModal()'></a></td>
		</tr>
	</table>


	<table id="tbParticipanteResult" border="1">
		<thead>
			<td>Matricula</td>
			<td>Nome</td>
			<td>Data</td>
			<td>Papel</td>
			<td>Acao</td>
		</thead>
		<tbody>	
			<?php foreach ($this->cart->contents() as $items): ?>
				<?php if($items['name'] == "participante"): ?>
					<tr>
						<td><?php echo $items['id']; ?></td>
						<td><?php echo $items['options']['nome']; ?></td>
						<td><?php echo $items['options']['data']; ?></td>
						<td><?php echo $items['options']['papel']; ?></td>
						<td><a href="removeParticipante/<?php echo $items['rowid']; ?>">remover</a></td>
					</tr>	
				<?php endif; ?>
			<?php endforeach; ?>
		</tbody>
	</table>


</div>
</body>

<script type="text/javascript">

function autocEdital() {
	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('#edital_id').val();
	if (keyword.length >= min_length) {
		$.ajax({
			url: 'getEdital',
			type: 'POST',
			data: {keyword:keyword},
			success:function(data){
				$('#edital_list_id').show();
				$('#edital_list_id').html(data);
			}
		});
	} else {
		$('#edital_list_id').hide();
	}
}

function novaLinhaEdital() {

	var dado = $('#edital_id').val();
	
	if (dado != "") {
		$.ajax({
			url: 'setEditalSessao',
			type: 'POST',
			data: {dadoEdital:dado},
			success:function(data){
				location.reload(); 
			}
		});
	} else {
		alert("Selecione um edital válido");
		$('#edital_list_id').hide();
	}
}

// set_item : this function will be executed when we select an item
function set_item_edital(item) {

	// change input value
	$('#edital_id').val(item);
	// hide proposition list
	$('#edital_list_id').hide();
}


function autocProfessor() {
	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('#professor_id').val();
	if (keyword.length >= min_length) {
		$.ajax({
			url: 'getProfessor',
			type: 'POST',
			data: {keyword:keyword},
			success:function(data){
				$('#professor_list_id').show();
				$('#professor_list_id').html(data);
			}
		});
	} else {	
		$('#professor_list_id').hide();
	}
}

function novaLinhaProfessor() {

	var professor = $('#professor_id').val();
	var papel = $('#paprof').val();
	var data = $('#dt_prof').val();

	var sendInfo = {
		nome: professor,
		papel: papel,
		data: data
	};
	
	if (papel != "erro" && professor != "" &&  data != "") { 		
		$.ajax({
			url: 'setProfessorSessao',
			// dataType: "json",
			type: 'POST',
			data: {dadosProfessor:sendInfo},
			success: function() {
    			window.location.reload(true);
			}
		});		
	} else {
		alert("Preencher os dados do Professor Corretamente");
		$('#edital_list_id').hide();
	}
}

// set_item : this function will be executed when we select an item
function set_item_professor(item) {

	// change input value
	$('#professor_id').val(item);
	// hide proposition list
	$('#professor_list_id').hide();
}

function novaLinhaParticipante() {

	var participante = $('#participante_id').val();
	var papel = $('#paparticipante').val();
	var data = $('#dt_participante').val();

	var sendInfo = {
		nome: participante,
		papel: papel,
		data: data
	};
	
	if (papel != "erro" && participante != "" &&  data != "") { 		
		$.ajax({
			url: 'setParticipanteSessao',
			// dataType: "json",
			type: 'POST',
			data: {dadosParticipante:sendInfo},
			success: function() {
    			window.location.reload(true);
			}
		});		
	} else {
		alert("Preencher os dados do Participante Corretamente");
		$('#participante_list_id').hide();
	}
}

// set_item : this function will be executed when we select an item
function set_item_participante(item) {

	// change input value
	$('#participante_id').val(item);
	// hide proposition list
	$('#participante_list_id').hide();
}

function autocParticipante() {
	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('#participante_id').val();
	if (keyword.length >= min_length) {
		$.ajax({
			url: 'getAlunos',
			type: 'POST',
			data: {keyword:keyword},
			success:function(data){
				$('#participante_list_id').show();
				$('#participante_list_id').html(data);
			}
		});
	} else {
		$('#participante_list_id').hide();
	}
}	
</script>

</html>