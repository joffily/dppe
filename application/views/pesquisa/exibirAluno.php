<style type="text/css">
        
  tbody{
    width: 100%; 
    padding: 10px;
  }
  th{
    background-color:#EDEDEE; 
    border: 2px solid #A9A9A9; 
  }
  td{
    width: 100%;     
    background-color:#fff; 
    border: 2px solid #A9A9A9;
  }

</style>


<div>
  <br><br>  
  <p>Consulta > Pesquisar por Aluno > Visualizar Aluno </p>
  <h2>Visualizar Aluno</h2>
  <br>
</div>
<div class="empresa">

  <?php if(!empty($aluno[0]['id'])): ?>

    <table id="example" class="table display table-responsive dataTable" width="100%" cellspacing="0">
      <tbody >
        <tr>
          <th>Matrícula</th>
          <td><?php echo $aluno[0]['matricula']; ?></td>
        </tr>
         <tr>
          <th>Nome</th>
          <td><?php echo $aluno[0]['nome']; ?></td>
        </tr>
         <tr>
         <th>Curso</th>
          <td><?php echo $aluno[0]['curso']; ?></td>
        </tr>
         <tr>
          <th style="padding-right:10px;">Situação da Matrícula</th>  
          <td><?php echo $aluno[0]['situacao_matricula'];?></td>
        </tr>
      </tbody>
    </table>

   <?php endif ?>
</div>


