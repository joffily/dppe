<style type="text/css">
        
  tbody{
    width: 100%; 
    padding: 10px;
  }
  th{
    background-color:#EDEDEE; 
    border: 2px solid #A9A9A9; 
  }
  td{
    width: 100%; 
    background-color:#fff; 
    border: 2px solid #A9A9A9;
  }

</style>

<div>
  <br><br>  
  <p>Consulta > Pesquisar por Professor > Visualizar Professor </p>
  <h2>Visualizar Professor</h2>
  <br>
</div>
<div class="empresa">
  <?php if(!empty($professor[0]['id'])): ?>
    <table id="example" class="table display table-responsive dataTable" width="100%" cellspacing="0"> 
      <tbody>                     
        <tr>
          <th>Matrícula</th>
          <td><?php echo $professor[0]['siape']; ?></td>
        </tr>  
        <tr> 
          <th>Nome</th>  
          <td><?php echo $professor[0]['nome']; ?></td> 
        </tr> 
        <tr>
          <th>Sexo</th>  
          <td><?php echo $professor[0]['sexo']; ?></td>
        </tr>
        <tr>
          <th style="padding-right:10px;">Titularidade</th>  
          <td><?php echo $professor[0]['titularidade']; ?></td>
        </tr>   
        <tr>  
          <th>Situação</th>
          <td><?php echo $professor[0]['situacao']; ?></td> 
        </tr> 
        <tr>
          <th>Instituição</th> 
          <td><?php echo $professor[0]['instituicao'];?></td>
        </tr>    
      </tbody>
    </table>
  <?php endif ?>
</div>