<div>
	<br><br>	
	<p>Consulta > Pesquisa por Aluno</p>
	<h2>Pesquisa por Alunos</h2>
</div>
<br>
<div>
<div style=" background-color:#EEEEEE; position: relative; -moz-border-radius:4px; -webkit-border-radius:4px; 
 border-radius:4px; border: 2px solid #D1D1D1; ">
	<div style=" font-weight: bold; padding-top:12px; padding-left:12px; border-bottom: 2px solid #D1D1D1; ">
		Pesquisa por Alunos
	</div>
	<?php 
		echo form_open("pesquisa/pesquisaAluno");
	?>
	<div style="  background-color:#fff; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		Nome:
		<div class='form-group' style='padding-right: 15px'>
		<?php 
			echo form_input( array(
						"name" => "nome",
						"class" => "form-control",
						"maxlength" => "70",
						"id" => "nome",
						"placeholder" => "Nome"
				));
		?>
		</div>
	</div>
	<div style=" padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		Matrícula:
		<div class='form-group' style='padding-right: 15px'>
		<?php 
			echo form_input( array(
						"name" => "matricula",
						"class" => "form-control",
						"maxlength" => "70",
						"id" => "matricula",
						"placeholder" => "Matrícula"
				));
		?>
		</div>
	</div>
	<div style="background-color:#fff; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		Situação da Matrícula:
		<div class='form-group' style='padding-right: 15px'>
		<?php 
			echo form_dropdown("situacao_matricula", $situacao, 'Situação da Matrícula' ,'class="form-control"');
		?>
		</div>
	</div>
	<div style=" padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		Curso:
		<div class='form-group' style='padding-right: 15px'>
		<?php 
			echo form_dropdown("curso", $curso, 'Curso' ,'class="form-control"');
		?>
		</div>
	</div>

	<div style="background-color:#fff; padding-top:12px; padding-bottom:1px;">
	  <div style="background-color:#fff; padding-bottom:1px; border-top: 1px solid  #D1D1D1;">		
		<br>
		<div class='form-group' style='text-align:center'>
			<table>
				<tr>
				  <td style="padding-left:20px">
					<?php
						echo form_button(array(
							"class" => "btn btn-primary",
							"content" => "Pesquisar",
							"type" => "submit"
						));
					?>
				  </td>
				  <td style="padding-left:40px">
					<?php
						echo form_button(array(
							"class" => "btn btn-info",
							"content" => "Limpar",
							"type" => "reset"
						));
					?>
				  </td>
				 </tr>
			</table>
		</div>
	  </div>
	</div>	
	<?php echo form_close(); ?>
</div>
</div>

<br><br>

 <?php if(isset($alunos)): ?> 
 <!-- Resultado da pesquisa -->
<div style=" min-height: 100%; background-color:#d9d9d9;  position: relative; -moz-border-radius:4px; -webkit-border-radius:4px; 
 border-radius:4px; ">

    <table id="example" class="table display table-responsive dataTable" width="100%" cellspacing="0">
     <thead style=" min-height: 100%;background-color:#EDEDEE;" >
        <tr>
          <th >Matrícula</th>
          <th>Nome</th>
          <th>Curso</th>
          <th>Situação da Matrícula</th>          
        </tr>
      </thead>  
      <tbody style="width: 100%;">
            
        <?php foreach($alunos as $linha): ?>                
        <tr>
          <td align="center" style="width: 15%;"><a href=<?php echo"exibirAluno/".$linha['id']; ?>><?php echo $linha['matricula']; ?></a></td>
          <?php if(strlen($linha['nome']) > 30) : ?>				
				<td style="width: 25%;"><?= substr($linha['nome'], 0, 30);  ?>...</td>
		  <?php endif ?>
		  <?php if(strlen($linha['nome']) <= 30) : ?>				
				<td style="width: 25%;"><?php echo $linha['nome']; ?></td> 
		  <?php endif ?>    
		  <?php if(strlen($linha['curso']) > 30) : ?>				
				<td style="width: 25%;"><?= substr($linha['curso'], 0, 30);  ?>...</td>
		  <?php endif ?>
		  <?php if(strlen($linha['curso']) <= 30) : ?>				
				<td style="width: 25%;"><?php echo $linha['curso']; ?></td> 
		  <?php endif ?>      
          <td style="text-align:center; width:100%"><?php echo $linha['situacao_matricula'];?></td>
        </tr>      
        <?php endforeach ?>      
      </tbody>
    </table>
</div>
<?php endif ?>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').dataTable({
    	 "pagingType": "full_numbers"
    });
} );
</script>
