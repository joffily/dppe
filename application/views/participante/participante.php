<div>
	<br><br>	
	<p>Registro > Gerenciar Participante > Visualizar Participante</p>
	<h2>Visualizar Participante</h2>
</div>
<br>
<div>
<div style=" background-color:#EEEEEE; position: relative; -moz-border-radius:4px; -webkit-border-radius:4px; 
 border-radius:4px; border: 2px solid #D1D1D1; ">
	 <div style=" font-weight: bold; padding-top:12px; padding-left:12px; border-bottom: 2px solid #D1D1D1; ">
		<b>Cadastro Participante</b>
	</div>
	<div style="  background-color:#fff; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Nome:</b>
		<div class='form-group' style='padding-right: 15px'>
			<?php 
			if(strlen($participante[0]['nome']) != 0){
				echo $participante[0]['nome'];
			}else{echo "--";}
			?>
		</div>
	</div>	
	<div style=" background-color:#EEEEEE; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>CFP:</b>	
		<div class='form-group' style='padding-right: 15px'>
		<?php
			if(strlen($participante[0]['cpf']) != 0){
				echo $participante[0]['cpf'];
			}else{echo "--";}
		?>
		</div>
	</div>	
	<div style=" background-color:#EEEEEE; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Identidade:</b>	
		<div class='form-group' style='padding-right: 15px'>
		<?php					
			if(strlen($participante[0]['identidade']) != 0){
				echo $participante[0]['identidade'];
			}else{echo "--";}
		?>
		</div>
	</div>	
	<div style=" background-color:#fff; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Órgão Emissor:</b>	
		<div class='form-group' style='padding-right: 15px'>
		<?php
			if(strlen($participante[0]['org_emissor']) != 0){
				echo $participante[0]['org_emissor'];
			}else{echo "--";}
		?>
		</div>
	</div>	
	<div style=" background-color:#fff; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Órgão Emissor - UF:</b>	
		<div class='form-group' style='padding-right: 15px'>
		<?php					
			if(strlen($participante[0]['org_emissor_uf']) != 0){
				echo $participante[0]['org_emissor_uf'];
			}else{echo "--";}
		?>
		</div>
	</div>	
	<div style=" background-color:#EEEEEE; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Data Nascimento:</b>	
		<div class='form-group' style='padding-right: 15px'>
		<?php
			if(strlen($participante[0]['dt_nascimento']) != 0){
				echo $participante[0]['dt_nascimento'];
			}else{echo "--";}
		?>
		</div>
	</div>	
	<div style=" background-color:#EEEEEE; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Tipo:</b>	
		<div class='form-group' style='padding-right: 15px'>
		<?php					
			if(strlen($participante[0]['tipo']) != 0){
				echo $participante[0]['tipo'];
			}else{echo "--";}
		?>
		</div>
	</div>	
	<div style="  background-color:#fff; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Curso:</b>
		<div class='form-group' style='padding-right: 15px'>
			<?php 
			if(strlen($participante[0]['curso']) != 0){
				echo $participante[0]['curso'];
			}else{echo "--";}
			?>
		</div>
	</div>	
	<div style=" background-color:#EEEEEE; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Matrícula:</b>	
		<div class='form-group' style='padding-right: 15px'>
		<?php
			if(strlen($participante[0]['matricula']) != 0){
				echo $participante[0]['matricula'];
			}else{echo "--";}
		?>
		</div>
	</div>	
	<div style=" background-color:#EEEEEE; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Situação da Matrícula:</b>	
		<div class='form-group' style='padding-right: 15px'>
		<?php					
			if(strlen($participante[0]['situacao_matricula']) != 0){
				echo $participante[0]['situacao_matricula'];
			}else{echo "--";}	
		?>
		</div>
	</div>	
	<div style="  background-color:#fff; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Logradouro:</b>
		<div class='form-group' style='padding-right: 15px'>
			<?php 
				if(strlen($participante[0]['logradouro']) != 0){
					echo $participante[0]['logradouro'];
				}else{echo "--";}
			?>
		</div>
	</div>
	<div style=" background-color:#EEEEEE; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Número:</b>	
		<div class='form-group' style='padding-right: 15px'>
		<?php
			if(strlen($participante[0]['numero']) != 0){
				echo $participante[0]['numero'];
			}else{echo "--";}
		?>
		</div>
	</div>	
	<div style=" background-color:#EEEEEE; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Complemento:</b>	
		<div class='form-group' style='padding-right: 15px'>
		<?php				
			if(strlen($participante[0]['complemento']) != 0){
				echo $participante[0]['complemento'];
			}else{echo "--";}	
		?>
		</div>
	</div>	
	<div style=" background-color:#fff; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Cep:</b>	
		<div class='form-group' style='padding-right: 15px'>
		<?php
			if(strlen($participante[0]['cep']) != 0){
				echo $participante[0]['cep'];
			}else{echo "--";}
		?>
		</div>
	</div>	
	<div style=" background-color:#fff; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Bairro:</b>	
		<div class='form-group' style='padding-right: 15px'>
		<?php					
			if(strlen($participante[0]['bairro']) != 0){
				echo $participante[0]['bairro'];
			}else{echo "--";}
		?>
		</div>
	</div>	
	<div style=" background-color:#EEEEEE; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Cidade:</b>	
		<div class='form-group' style='padding-right: 15px'>
		<?php
			if(strlen($participante[0]['cidade']) != 0){
				echo $participante[0]['cidade'];
			}else{echo "--";}
		?>
		</div>
	</div>	
	<div style=" background-color:#EEEEEE; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Estado:</b>	
		<div class='form-group' style='padding-right: 15px'>
		<?php					
			if(strlen($participante[0]['estado']) != 0){
				echo $participante[0]['estado'];
			}else{echo "--";}
		?>
		</div>
	</div>	
		<div style=" background-color:#fff; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Banco:</b>	
		<div class='form-group' style='padding-right: 15px'>
		<?php
			if(strlen($participante[0]['banco']) != 0){
				echo $participante[0]['banco'];
			}else{echo "--";}
		?>
		</div>
	</div>	
	<div style=" background-color:#fff; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Conta:</b>	
		<div class='form-group' style='padding-right: 15px'>
		<?php					
			if(strlen($participante[0]['conta']) != 0){
				echo $participante[0]['conta'];
			}else{echo "--";}
		?>
		</div>
	</div>	
	<div style=" background-color:#EEEEEE; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Agência:</b>	
		<div class='form-group' style='padding-right: 15px'>
		<?php
			if(strlen($participante[0]['agencia']) != 0){
				echo $participante[0]['agencia'];
			}else{echo "--";}
		?>
		</div>
	</div>	
	<div style=" background-color:#EEEEEE; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Operação:</b>	
		<div class='form-group' style='padding-right: 15px'>
		<?php					
			if(strlen($participante[0]['operacao']) != 0){
				echo $participante[0]['operacao'];
			}else{echo "--";}
		?>
		</div>
	</div>
	<div style="background-color:#fff; padding-top:12px; padding-left:12px; padding-bottom:1px;">
		<b>Registros:</b>
		</br></br>
		<?php if(count($registro) != 0) : ?>
			<table class="table" style="width: 90%; margin-left: 7px;">
				<tr>
					<th>Papel</th>
					<th colspan="3">Nome Registro</th>
					<th>Data de Início</th>
					<th>Data Fim</th>
					<th>Edital</th>
				</tr>
				
				<?php
					for ($i=0; $i < count($registro) ; $i++) { 
						switch ($registro[$i]['papel']) {
							case '1':
								$papel = "Orientador";
								break;
							case '2':
								$papel = "Bolsista";
								break;
							case '3':
								$papel = "Voluntário";
								break;
							case '4':
								$papel = "Servidor";
								break;						
						}
						?>
						<tr><td>
						<?php echo $papel;?>
						</td><td colspan='3'>
							<?php if(strlen($registro[$i]['titulo']) > 50) : ?>
							<a href="<?php echo base_url()."index.php/registro/mostra/{$registro[$i]['id']}" ?>"><?= substr($registro[$i]['titulo'], 0, 50);  ?>...</a>				
							<?php endif ?>
							<?php if(strlen($registro[$i]['titulo']) <= 50) : ?>				
							<a href="<?php echo base_url()."index.php/registro/mostra/{$registro[$i]['id']}" ?>"><?= $registro[$i]['titulo']  ?></a>				
							<?php endif ?>
						</td><td>
						<?php echo date('d/m/Y', strtotime($registro[$i]['dt_inicio']));?>
						</td><td>
						<?php if($registro[$i]['dt_fim'] != "--"){
						echo date('d/m/Y', strtotime($registro[$i]['dt_fim']));
						}
						if($registro[$i]['dt_fim'] == "--"){
						echo $registro[$i]['dt_fim'];
						}
						?>
						</td>						
						<td>
							<a href="<?php echo base_url()."index.php/edital/visualizaredital/{$registro[0]['id_edital']}" ?>"><?=$registro[0]['numeroedital'] ?>/<?=$registro[0]['anoedital'] ?></a>				
		
						
						</td>
						</tr>
						<?php
					}
				?>
			</table>	
			<?php else: ?>
			<div class='form-group' style='padding-right: 15px'>
			<?php
				echo "--";
			?>
			</div>
			<?php endif ?>
		</div>
	</div>
</div>
