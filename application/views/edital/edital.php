<div>
	<br><br>
	<p>Edital > Gerenciar Edital > Visualizar Edital</p>
	<h2>Visualizar Edital</h2>
</div>
<br>
<div>
<div style=" background-color:#EEEEEE; position: relative; -moz-border-radius:4px; -webkit-border-radius:4px;
 border-radius:4px; border: 2px solid #D1D1D1; ">
	 <div style=" font-weight: bold; padding-top:12px; padding-left:12px; border-bottom: 2px solid #D1D1D1; ">
		<b>Cadastro Edital</b>
	</div>
	<div style=" background-color:#fff; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Número:</b>
		<div class='form-group' style='padding-right: 15px'>
		<?php
			if(strlen($edital[0]['numero']) != 0){
				echo $edital[0]['numero'];
			}else{echo "--";}
		?>
		</div>
	</div>
	<div style="background-color:#fff; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Tipo de Edital:</b>
		<div class='form-group' style='padding-right: 15px'>
		<?php
			if(strlen($dadosextras[0]['tipo']) != 0){
				echo $dadosextras[0]['tipo'];
			}else{echo "--";}
		?>
		</div>
	</div>
	<div style=" background-color:#EEEEEE; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Ano:</b>
		<div class='form-group' style='padding-right: 15px'>
		<?php
			if(strlen($dadosextras[0]['ano']) != 0){
				echo $dadosextras[0]['ano'];
			}else{echo "--";}
		?>
		</div>
	</div>
	<div style=" background-color:#EEEEEE; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Data Publicação:</b>
		<div class='form-group' style='padding-right: 15px'>
		<?php
			if(strlen($edital[0]['dt_publicacao']) != 0){
				echo $edital[0]['dt_publicacao'];
			}else{echo "--";}
		?>
		</div>
	</div>
	<div style="background-color:#fff; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Programa Financiador:</b>
		<div class='form-group' style='padding-right: 15px'>
		<?php
			if(strlen($dadosextras[0]['programa']) != 0){
				echo $dadosextras[0]['programa'];
			}else{echo "--";}
		?>
		</div>
	</div>
	<div style="background-color:#fff; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Financiadora Recurso:</b>
		<div class='form-group' style='padding-right: 15px'>
		<?php
			if(strlen($dadosextras[0]['frecurso']) != 0){
				echo $dadosextras[0]['frecurso'];
			}else{echo "--";}
		?>
		</div>
	</div>
	<div style=" background-color:#EEEEEE; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Total de Bolsas:</b>
		<div class='form-group' style='padding-right: 15px'>
		<?php
			if(strlen($edital[0]['total_bolsa']) != 0){
				echo $edital[0]['total_bolsa'];
			}else{echo "--";}
		?>
		</div>
	</div>	
	<div style=" background-color:#EEEEEE; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Valor Bolsa:</b>
		<div class='form-group' style='padding-right: 15px'>
		<?php
			if(strlen($edital[0]['valor_bolsa']) != 0){
				echo $edital[0]['valor_bolsa'];
			}else{echo "--";}
		?>
		</div>
	</div>
	<div style="background-color:#fff; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Taxa de Bancada:</b>
		<div class='form-group' style='padding-right: 15px'>
		<?php
			if(strlen($edital[0]['taxa_bancada']) != 0){
				echo $edital[0]['taxa_bancada'];
			}else{echo "--";}
		?>
		</div>
	</div>
	<div style="background-color:#fff; float:left; width:50%; padding-top:12px; padding-left:12px; padding-bottom:1px; ">
		<b>Arquivo Digital:</b>
		<div class='form-group' style='padding-right: 15px'>
			<?php
			if(strlen($edital[0]['edital_url']) != 0){
			?>
			<a href="<?php echo base_url()."assets/uploads/files/".$edital[0]['edital_url']; ?>"><?php echo $edital[0]['edital_url']; ?></a>
			<?php
			}else{echo "--";}
			?>
		</div>
	</div>
	<div style=" background-color:#EEEEEE; padding-top:16px; padding-left:12px; padding-bottom:1px; ">
		<b>Objetivos Gerais:</b>
		<div class='form-group' style='padding-right: 15px'>
			<?php
				if(strlen($edital[0]['objetivos_gerais']) != 0){
					echo $edital[0]['objetivos_gerais'];
				}else{echo "--";}
			?>
		</div>
	</div>
	<div style="background-color:#fff; padding-top:12px; padding-left:12px; padding-bottom:1px;">
		<b>Registros:</b>
		</br></br>
		<?php if(count($registro) != 0) : ?>
			<table class="table" style="width: 90%; margin-left: 7px;">
				<tr>
					<th colspan="3">Nome Registro</th>
					<th>Data de Início</th>
					<th>Data Fim</th>
				</tr>
				<tbody>
				<?php
					for ($i=0; $i < count($registro) ; $i++) {
				?>
						<tr>
						<td colspan='3'>
							<?php if(strlen($registro[$i]['titulo']) > 50) : ?>
							<a href="<?php echo base_url()."index.php/registro/mostra/{$registro[$i]['id']}" ?>"><?= substr($registro[$i]['titulo'], 0, 50);  ?>...</a>
							<?php endif ?>
							<?php if(strlen($registro[$i]['titulo']) <= 50) : ?>
							<a href="<?php echo base_url()."index.php/registro/mostra/{$registro[$i]['id']}" ?>"><?= $registro[$i]['titulo']  ?></a>
							<?php endif ?>
						</td><td>
						<?php echo date('d/m/Y', strtotime($registro[$i]['dt_inicio']));?>
						</td><td>
						<?php if($registro[$i]['dt_fim'] != "--"){
						echo date('d/m/Y', strtotime($registro[$i]['dt_fim']));
						}
						if($registro[$i]['dt_fim'] == "--"){
						echo $registro[$i]['dt_fim'] ;
						}
						?>
						</td>
						</tr>
						<?php
					}
				?>
			</tbody>
			</table>
			<?php else: ?>
			<div class='form-group' style='padding-right: 15px'>
			<?php
				echo "--";
			?>
			</div>
			<?php endif ?>
		</div>
	</div>
</div>
