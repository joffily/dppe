$(document).ready(function() {
    var edital = {}

    edital.save = function(key, value) {
        $.Storage.saveItem(key, value);
    };
    
    edital.load = function(key) {
        return $.Storage.loadItem(key)
    };

    edital.loadFields = function() {
        var fields = ["titulo", "descricao", "dt_inicio", "dt_fim"];
        for(id in fields) {
            var field = $("#"+fields[id]),
                text = edital.load(fields[id]);
            if (field && text.length > 0) {
                $("#"+fields[id])[0].value = text;
            }
        }
    }

    edital.clean = function() {
        var fields = ["titulo", "descricao", "dt_inicio", "dt_fim"];
        for(id in fields) {
            edital.save(fields[id], "");
        }
    }

    $("#titulo, #dt_inicio, #dt_fim, #descricao").on("change", function(e) {
        edital.save(this.id, this.value);
        edital.save('hasData', true);
        console.log("Salvando: " + this.id);
    })

    $("#registro-salvar").on("click", function(e) {
        edital.clean();
    })

    if (window.location.pathname.search("registro/novo") != -1 || 
        window.location.pathname.search("registro/cadastro") != -1) {
        if ($.Storage.loadItem("hasData")) {
            edital.loadFields();
            console.log("Populando formulário..");
        }
    }

});

$(document).ready(function() {
    var $janela = $("#cadastro-info");
})


$(document).ready(function() {
    var $fileInput = $("#id-projeto-arquivo");
    var $labelInputName = $(".file-upload-label #name")[0];

    $fileInput.on("change", function(e) {
        var el = $(this)[0];
        for (var i = 0; i < el.files.length; i++) {
            $labelInputName.textContent = el.files[i].name;
        }
    });
});
