// autocomplet : this function will be executed every time we change the text
function autocomplet() {
	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('#participante_id').val();
	if (keyword.length >= min_length) {
		$.ajax({
			url: '../ajax/getAlunos',
			type: 'POST',
			data: {keyword:keyword},
			success:function(data){
				$('#participante_list_id').show();
				$('#participante_list_id').html(data);
			}
		});
	} else {
		$('#participante_list_id').hide();
	}
}

// set_item : this function will be executed when we select an item
function set_item(item) {
	// change input value
	$('#participante_id').val(item);
	// hide proposition list
	$('#participante_list_id').hide();
}

// autocomplet : this function will be executed every time we change the text
function autocEdital() {
	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('#edital_id').val();
	if (keyword.length >= min_length) {
		$.ajax({
			url: '../ajax/getEdital',
			type: 'POST',
			data: {keyword:keyword},
			success:function(data){
				$('#edital_list_id').show();
				$('#edital_list_id').html(data);
			}
		});
	} else {
		$('#edital_list_id').hide();
	}
}

// set_item : this function will be executed when we select an item
function set_item_edital(item) {

	// change input value
	$('#edital_id').val(item);
	// hide proposition list
	$('#edital_list_id').hide();
}




// autocomplet : this function will be executed every time we change the text
function autocProfessor() {
	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('#professor_id').val();
	if (keyword.length >= min_length) {
		$.ajax({
			url: '../ajax/getProfessor',
			type: 'POST',
			data: {keyword:keyword},
			success:function(data){
				$('#professor_list_id').show();
				$('#professor_list_id').html(data);
			}
		});
	} else {
		$('#professor_list_id').hide();
	}
}

// set_item : this function will be executed when we select an item
function set_item_professor(item) {
	// change input value
	$('#professor_id').val(item);
	// hide proposition list
	$('#professor_list_id').hide();
}