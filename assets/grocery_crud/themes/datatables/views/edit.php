<?php

	$this->set_css($this->default_theme_path.'/datatables/css/datatables.css');

    $this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.form.min.js');
	$this->set_js_config($this->default_theme_path.'/datatables/js/datatables-edit.js');
	$this->set_css($this->default_css_path.'/ui/simple/'.grocery_CRUD::JQUERY_UI_CSS);
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/ui/'.grocery_CRUD::JQUERY_UI_JS);

	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
?>

<?php if(strcmp($subject,"Edital") == 0): ?>
<div>
	<br><br>
	<p>Edital > Gerenciar Edital > Editar Edital</p>
	<h2>Editar Edital</h2><br>
</div>
<?php endif ?>

<?php if(strcmp($subject,"Participante") == 0): ?>
<div>
	<br><br>
	<p>Participante > Gerenciar Participante > Editar Participante</p>
	<h2>Editar Participante</h2><br>
</div>
<?php endif ?>

<?php if(strcmp($subject,"Usuário") == 0): ?>
<div>
	<br><br>
	<p>Sistema > Gerenciar Usuários > Editar Usuário</p>
	<h2>Editar Usuário</h2><br>
</div>
<?php endif ?>

<div class='ui-widget-content ui-corner-all datatables'>
	<h3 class="ui-accordion-header ui-helper-reset ui-state-default form-title">
		<div class='floatL form-title-left'>
			<a href="#"><?php echo $this->l('form_edit'); ?> <?php echo $subject?></a>
		</div>
		<div class='clear'></div>
	</h3>
<div class='form-content form-div'>
	<?php echo form_open( $update_url, 'method="post" id="crudForm" enctype="multipart/form-data"'); ?>
		<div>
		<?php
			if(strcmp($subject,"Edital") == 0): ?>
			<style type="text/css">
					input[type="text"] {
							width: 300px !important;
					}

					* { box-sizing: border-box; }
					.form-row {
						float: left;
						width: 100%;
						clear: both;
					}
					.form-row:nth-child(2n+1) {
						background: #eee;
						border-bottom: 1px solid #ddd;
						border-top: 1px solid #ddd;
					}
					.form-input {
						float: left;
						width: 50%;
						clear: right;
					}

					.form-input:after,
					.form-input:before,
					.form-row:after,
					.form-row:before {
						content: "";
						display: table;
						clear: both;
					}

					#report-error.report-div.error {
						float: left;
						clear: both;
						width: 100%;
					}

					#report-success.report-div.success {
						float: left;
						clear: both;
						width: 100%;
					}
			</style>
			<?php
				// Edital
				$counter = 0;
				// Determinamos quais campos não queremos exibir no par-par
				$shouldNotList = array('Descrição');
				// Array para guardar campos não exibidos
				$showAtEndFields = array();
				$addOneToEnd = (count($fields) - count($shouldNotList)) % 2 == 0 ? false : true;
				foreach($fields as $field) {
					$even_odd = $counter % 2 == 0 ? 'odd' : 'even';
					$counter++;
			?>
				<?php $rr = $input_fields[$field->field_name]->display_as; ?>

				<?php
				 	// verifica quais campos podem ser exibidos na tela de forma par-par
					if (!in_array($rr, $shouldNotList)):
				?>
					<?php if ($counter % 2 != 0): ?>
						<div class='form-row form-field-box' id="<?php echo $field->field_name; ?>_field_box">
					<?php endif; ?>
							<div class="form-input">
								<div id="<?php echo $field->field_name; ?>_display_as_box">
									<?php echo $input_fields[$field->field_name]->display_as?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""?> :
								</div>
								<div id="<?php echo $field->field_name; ?>_input_box">
									<?php echo $input_fields[$field->field_name]->input?>
								</div>
							</div>
					<?php if ($counter%2 == 0): ?>
						</div>
					<?php endif; ?>
				<?php else:
					$counter--;
					array_push($showAtEndFields, $field);
				?>
				<?php endif; ?>
			<?php }?>
			<?php if ($addOneToEnd): ?>
			</div>
			<?php endif; ?>
			<?php
			 	// Exibindo os campos que não foram exibidos no par-par
				foreach ($showAtEndFields as $field):
			?>
				<div class='form-row form-field-box' id="<?php echo $field->field_name; ?>_field_box">
					<div class="">
						<div id="<?php echo $field->field_name; ?>_display_as_box">
							<?php echo $input_fields[$field->field_name]->display_as?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""?> :
						</div>
						<div id="<?php echo $field->field_name; ?>_input_box">
							<?php echo $input_fields[$field->field_name]->input?>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
			<?php
			elseif(strcmp($subject,"Participante") == 0): ?>
			    <style type="text/css">
        			input[type="text"] {
            			width: 300px !important;
        			}
        			.localinputmaior input[type="text"] {
            			width: 600px !important;
        			}
   			 	</style>
			<?php
			$counter = 0;
				foreach($fields as $field)
				{
					$even_odd = $counter % 2 == 0 ? 'odd' : 'even';
					$counter++;
			?>


			<?php
			$rr = $input_fields[$field->field_name]->display_as;
			if(strcmp($rr,"Nome") != 0 && strcmp($rr,"Curso") != 0 && strcmp($rr,"Logradouro") != 0): ?>
 			<?php
	 			if(strcmp($rr,"Identidade") != 0 && strcmp($rr,"Órgão Emissor - UF") != 0 && strcmp($rr,"Tipo") != 0 && strcmp($rr,"Situação da Matrícula") != 0
	 				&& strcmp($rr,"Complemento") != 0 && strcmp($rr,"Bairro") != 0  && strcmp($rr,"Estado") != 0 && strcmp($rr,"Conta") != 0 && strcmp($rr,"Operação") != 0 ): ?>
				<div class='form-field-box <?php echo $even_odd?>' id="<?php echo $field->field_name; ?>_field_box">
				<div style="width: 50%; float:left">
					<div id="<?php echo $field->field_name; ?>_display_as_box">
						<?php echo $input_fields[$field->field_name]->display_as?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""?> :
					</div>
					<div id="<?php echo $field->field_name; ?>_input_box">
						<?php echo $input_fields[$field->field_name]->input?>
					</div>
				</div>
				<?php endif ?>
				<?php if(strcmp($rr,"Identidade") == 0 || strcmp($rr,"Órgão Emissor - UF") == 0 || strcmp($rr,"Tipo") == 0 || strcmp($rr,"Situação da Matrícula") == 0
						|| strcmp($rr,"Complemento") == 0 || strcmp($rr,"Bairro") == 0 || strcmp($rr,"Estado") == 0  || strcmp($rr,"Conta") == 0 || strcmp($rr,"Operação") == 0 ): ?>
				<div style="width: 50%; float:right">
					<div id="<?php echo $field->field_name; ?>_display_as_box">
						<?php echo $input_fields[$field->field_name]->display_as?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""?> :
					</div>
					<div id="<?php echo $field->field_name; ?>_input_box">
						<?php echo $input_fields[$field->field_name]->input?>
					</div>
				</div>
				<?php $counter--; endif ?>
				<?php
				$ss = $input_fields[$field->field_name]->display_as;
				if(strcmp($ss,"CPF") != 0 && strcmp($ss,"Órgão Emissor") != 0 && strcmp($ss,"Data Nascimento") != 0 && strcmp($ss,"Matrícula") != 0 && strcmp($ss,"Número") != 0
					&& strcmp($ss,"Cep") != 0 && strcmp($ss,"Cidade") != 0 && strcmp($ss,"Banco") != 0 && strcmp($ss,"Agência") != 0 ): ?>
				<div class='clear'></div>
				</div>
				<?php endif ?>
			<?php else: ?>
			<div class='form-field-box <?php echo $even_odd?>' id="<?php echo $field->field_name; ?>_field_box">
				<div style="width: 100%;">
					<div id="<?php echo $field->field_name; ?>_display_as_box">
						<?php echo $input_fields[$field->field_name]->display_as?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""?> :
					</div>
					<div class='localinputmaior ' id="<?php echo $field->field_name; ?>_input_box">
						<?php echo $input_fields[$field->field_name]->input?>
					</div>
					<div class='clear'></div>
				</div>
			</div>
			<?php endif ?>

			<?php }?>


			<?php else: ?>
			<?php
			$counter = 0;
				foreach($fields as $field)
				{
					$even_odd = $counter % 2 == 0 ? 'odd' : 'even';
					$counter++;
			?>
			<div class='form-field-box <?php echo $even_odd?>' id="<?php echo $field->field_name; ?>_field_box">
				<div class='form-display-as-box' id="<?php echo $field->field_name; ?>_display_as_box">
					<?php echo $input_fields[$field->field_name]->display_as?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""?> :
				</div>
				<div class='form-input-box' id="<?php echo $field->field_name; ?>_input_box">
					<?php echo $input_fields[$field->field_name]->input?>
				</div>
				<div class='clear'></div>
			</div>
			<?php }?>
			<?php endif ?>
			<!-- Start of hidden inputs -->
				<?php
					foreach($hidden_fields as $hidden_field){
						echo $hidden_field->input;
					}
				?>
			<!-- End of hidden inputs -->
			<?php if ($is_ajax) { ?><input type="hidden" name="is_ajax" value="true" /><?php }?>
			<div class='line-1px'></div>
			<div id='report-error' class='report-div error'></div>
			<div id='report-success' class='report-div success'></div>
		</div>
		<div class='buttons-box'>
			<div class='form-button-box'>
				<input  id="form-button-save" type='submit' value='<?php echo $this->l('form_update_changes'); ?>' class='ui-input-button' />
			</div>
			<?php 	if(!$this->unset_back_to_list) { ?>
			<div class='form-button-box'>
				<input type='button' value='<?php echo $this->l('form_update_and_go_back'); ?>' class='ui-input-button' id="save-and-go-back-button"/>
			</div>
			<div class='form-button-box'>
				<input type='button' value='<?php echo $this->l('form_cancel'); ?>' class='ui-input-button' id="cancel-button" />
			</div>
			<?php }?>
			<div class='form-button-box loading-box'>
				<div class='small-loading' id='FormLoading'><?php echo $this->l('form_update_loading'); ?></div>
			</div>
			<div class='clear'></div>
		</div>
	</form>
</div>
</div>
<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';

	var message_alert_edit_form = "<?php echo $this->l('alert_edit_form')?>";
	var message_update_error = "<?php echo $this->l('update_error')?>";
</script>
