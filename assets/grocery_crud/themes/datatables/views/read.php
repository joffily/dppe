<?php

	$this->set_css($this->default_theme_path.'/datatables/css/datatables.css');
	$this->set_js_lib($this->default_theme_path.'/flexigrid/js/jquery.form.js');
	$this->set_js_config($this->default_theme_path.'/datatables/js/datatables-edit.js');
	$this->set_css($this->default_css_path.'/ui/simple/'.grocery_CRUD::JQUERY_UI_CSS);
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/ui/'.grocery_CRUD::JQUERY_UI_JS);

	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.noty.js');
	$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/config/jquery.noty.config.js');
?>
<?php if(strcmp($subject,"Edital") == 0): ?> 
<div>
	<br><br>	
	<p>Edital > Gerenciar Edital > Visualizar Edital</p>
	<h2>Visualizar Edital</h2><br>
</div>
<?php endif ?>

<?php if(strcmp($subject,"Participante") == 0): ?> 
<div>
	<br><br>	
	<p>Participante > Gerenciar Participante > Visualizar Participante</p>
	<h2>Visualizar Participante</h2><br>
</div>
<?php endif ?>

<?php if(strcmp($subject,"Usuário") == 0): ?> 
<div>
	<br><br>	
	<p>Sistema > Gerenciar Usuários > Visualizar Usuário</p>
	<h2>Visualizar Usuário</h2><br>
</div>
<?php endif ?>

<div class='ui-widget-content ui-corner-all datatables'>
	<h3 class="ui-accordion-header ui-helper-reset ui-state-default form-title">
		<div class='floatL form-title-left'>
			<a href="#"><?php echo $this->l('list_record'); ?> <?php echo $subject?></a>
		</div>
		<div class='clear'></div>
	</h3>
<div class='form-content form-div'>
	<?php echo form_open( $read_url, 'method="post" id="crudForm"  enctype="multipart/form-data"'); ?>
		<div>
			<?php 
			if(strcmp($subject,"Participante") == 0): ?> 
			    <style type="text/css">
        			input[type="text"] {
            			width: 300px !important;
        			}        			
        			.localinputmaior input[type="text"] {
            			width: 600px !important;
        			}
   			 	</style>
			<?php
			$counter = 0;
				foreach($fields as $field)
				{
					$even_odd = $counter % 2 == 0 ? 'odd' : 'even';
					$counter++;
			?>


			<?php
			$rr = $input_fields[$field->field_name]->display_as; 
			if(strcmp($rr,"Nome") != 0 && strcmp($rr,"Curso") != 0 && strcmp($rr,"Logradouro") != 0): ?> 
 			<?php 
	 			if(strcmp($rr,"Órgão Emissor") != 0 && strcmp($rr,"Data Nascimento") != 0 && strcmp($rr,"CPF") != 0 && strcmp($rr,"Complemento") != 0 
	 						&& strcmp($rr,"Bairro") != 0  && strcmp($rr,"Estado") != 0 && strcmp($rr,"Conta") != 0 && strcmp($rr,"Operação") != 0 && strcmp($rr,"Tipo") != 0 ): ?> 
				<div class='form-field-box <?php echo $even_odd?>' id="<?php echo $field->field_name; ?>_field_box">
				<div style="width: 50%; float:left"> 
					<div id="<?php echo $field->field_name; ?>_display_as_box">
						<?php echo $input_fields[$field->field_name]->display_as?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""?> :
					</div>
					<div id="<?php echo $field->field_name; ?>_input_box">
						<?php echo $input_fields[$field->field_name]->input?>
					</div>
				</div>
				<?php endif ?>
				<?php if(strcmp($rr,"Órgão Emissor") == 0 || strcmp($rr,"Data Nascimento") == 0 || strcmp($rr,"CPF") == 0 || strcmp($rr,"Complemento") == 0 
						|| strcmp($rr,"Bairro") == 0 || strcmp($rr,"Estado") == 0  || strcmp($rr,"Conta") == 0 || strcmp($rr,"Operação") == 0 || strcmp($rr,"Tipo") == 0): ?> 
				<div style="width: 50%; float:right"> 
					<div id="<?php echo $field->field_name; ?>_display_as_box">
						<?php echo $input_fields[$field->field_name]->display_as?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""?> :
					</div>
					<div id="<?php echo $field->field_name; ?>_input_box">
						<?php echo $input_fields[$field->field_name]->input?>
					</div>
				</div>
				<?php $counter--; endif ?>
				<?php 
				$ss = $input_fields[$field->field_name]->display_as;
				if(strcmp($ss,"Identidade") != 0 && strcmp($ss,"Órgão Emissor - UF") != 0 && strcmp($ss,"Matrícula") != 0 && strcmp($ss,"Número") != 0 
					&& strcmp($ss,"Cep") != 0 && strcmp($ss,"Cidade") != 0 && strcmp($ss,"Banco") != 0 && strcmp($ss,"Agência") != 0  && strcmp($ss,"Situação da Matrícula") != 0 ):?> 
				<div class='clear'></div>
				</div>
				<?php endif ?>
			<?php else: ?>
			<div class='form-field-box <?php echo $even_odd?>' id="<?php echo $field->field_name; ?>_field_box">
				<div style="width: 100%;"> 
					<div id="<?php echo $field->field_name; ?>_display_as_box">
						<?php echo $input_fields[$field->field_name]->display_as?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""?> :
					</div>
					<div class='localinputmaior ' id="<?php echo $field->field_name; ?>_input_box">
						<?php echo $input_fields[$field->field_name]->input?>
					</div>
					<div class='clear'></div>
				</div>
			</div>
			<?php endif ?>
		
			<?php }?>


			<?php else: ?>
			<?php
			$counter = 0;
				foreach($fields as $field)
				{
					$even_odd = $counter % 2 == 0 ? 'odd' : 'even';
					$counter++;
			?>
			<div class='form-field-box <?php echo $even_odd?>' id="<?php echo $field->field_name; ?>_field_box">
				<div class='form-display-as-box' id="<?php echo $field->field_name; ?>_display_as_box">
					<?php echo $input_fields[$field->field_name]->display_as?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""?> :
				</div>
				<div class='form-input-box' id="<?php echo $field->field_name; ?>_input_box">
					<?php echo $input_fields[$field->field_name]->input?>
				</div>
				<div class='clear'></div>
			</div>
			<?php }?>
			<?php endif ?>
			<!-- Start of hidden inputs -->
				<?php
					foreach($hidden_fields as $hidden_field){
						echo $hidden_field->input;
					}
				?>
			<!-- End of hidden inputs -->
			<?php if ($is_ajax) { ?><input type="hidden" name="is_ajax" value="true" /><?php }?>
			<div class='line-1px'></div>
			<div id='report-error' class='report-div error'></div>
			<div id='report-success' class='report-div success'></div>
		</div>
		<div class='buttons-box'>
			<div class='form-button-box'>
				<input type='button' value='<?php echo $this->l('form_back_to_list'); ?>' class='ui-input-button back-to-list' id="cancel-button" />
			</div>
			<div class='clear'></div>
		</div>
	</form>
</div>
</div>
<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';

	var message_alert_edit_form = "<?php echo $this->l('alert_edit_form')?>";
	var message_update_error = "<?php echo $this->l('update_error')?>";
</script>
